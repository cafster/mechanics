package com.webaspiration.mechanics.Interface;

import retrofit2.Response;

/**
 * Created by shaan on 7/30/2017.
 */

@SuppressWarnings("DefaultFileTemplate")
public interface NetworkInteractor<T> {

    void handle(Response<T> o);

    void error(Throwable e);

}
