package com.webaspiration.mechanics.Events;

import android.location.Location;

/**
 * Created by shaan on 10/10/2017.
 */

public class LocationEvent {

    private Location location;

    public LocationEvent(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }

}
