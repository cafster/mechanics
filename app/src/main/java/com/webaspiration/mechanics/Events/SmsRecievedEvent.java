package com.webaspiration.mechanics.Events;

/**
 * Created by shaan on 7/7/2017.
 */

@SuppressWarnings("DefaultFileTemplate")
public class SmsRecievedEvent {

    private String otp;

    public SmsRecievedEvent(String otp) {
        this.otp = otp;
    }

    public String getOtp() {
        return otp;
    }
}
