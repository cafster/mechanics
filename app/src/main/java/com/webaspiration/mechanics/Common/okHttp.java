package com.webaspiration.mechanics.Common;


import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class okHttp {

    public static String doPost(Map<String, String> pairs, String api_url) {
        String dummyResponse = "{\"fields\":0}";
        String result = "";
        int count = 0;

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(10, TimeUnit.SECONDS);
        client.setReadTimeout(10, TimeUnit.SECONDS);

        try {

            List<String> key = new ArrayList<String>(pairs.keySet());

            List<String> list = new ArrayList<String>(pairs.values());
            List<String> strings = new ArrayList<String>();
            FormEncodingBuilder formEncodingBuilder = new FormEncodingBuilder();
            HttpUrl.Builder urlBuilder = HttpUrl.parse(api_url).newBuilder();
            for (int i = 0; i < key.size(); i++) {

                try {
                    formEncodingBuilder.add(key.get(i), list.get(i));
                    urlBuilder.addQueryParameter(key.get(i),list.get(i));
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
            String url=urlBuilder.toString();
            Request request = new Request.Builder().url(api_url).post(formEncodingBuilder.build()).build();
            Response response = client.newCall(request).execute();
            result = response.body().string();
            response.body().close();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }

        if (result == null) {
            return dummyResponse;
        }
        return result;

    }
}
