package com.webaspiration.mechanics.Common;

import com.google.gson.Gson;
import com.webaspiration.mechanics.Network.Model.Error;

import io.reactivex.Observable;
import io.reactivex.functions.Function;
import retrofit2.Response;

/**
 * Created by shaan on 9/8/2017.
 */

@SuppressWarnings("DefaultFileTemplate")
public class Validates implements Function<Response,Observable<Response>> {

    public static Validates handle(){
        return new Validates();
    }

    @Override
    public Observable<Response> apply(Response response) throws Exception {
        if(response.code()==412){
            return Observable.error(new Exception("Invalid Session. Please login again to continue"));
        }
        else if(response.code()==409) {
            Error error=new Gson().fromJson(response.errorBody().string(), Error.class);
            return Observable.error(new Exception(error.getError()));
        }
        else
            return Observable.just(response);
    }


}
