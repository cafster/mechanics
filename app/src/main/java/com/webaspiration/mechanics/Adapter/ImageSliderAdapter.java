package com.webaspiration.mechanics.Adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.squareup.picasso.Picasso;
import com.webaspiration.mechanics.Network.Beans.ProductImage;
import com.webaspiration.mechanics.R;

import java.util.ArrayList;
import java.util.List;

public class ImageSliderAdapter extends PagerAdapter {

    List<ProductImage> arrayList;
    Context _context;
    LayoutInflater layoutInflater;

    public ImageSliderAdapter(Context context, List<ProductImage> arrayList){
        _context=context;
        this.arrayList=arrayList;

    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view==(LinearLayout)object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout)object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater=(LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view=layoutInflater.inflate(R.layout.imageslider,container,false);
        ImageView imageView=(ImageView)view.findViewById(R.id.sliderImage);
        RequestOptions requestOptions=new RequestOptions().placeholder(R.drawable.eventplaceholder);
        Glide.with(_context).applyDefaultRequestOptions(requestOptions).load(arrayList.get(position).getPath()).into(imageView);
        container.addView(view);
        return view;
    }
}
