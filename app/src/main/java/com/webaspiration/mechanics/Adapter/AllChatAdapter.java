package com.webaspiration.mechanics.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.webaspiration.mechanics.Common.Common;
import com.webaspiration.mechanics.Dagger.Provides.Fonts;
import com.webaspiration.mechanics.Network.Beans.ChatResponse.Chat;
import com.webaspiration.mechanics.Network.Beans.Product;
import com.webaspiration.mechanics.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AllChatAdapter extends RecyclerView.Adapter<AllChatAdapter.ViewHolder> {

    private Context mContext;
    private List<Chat> arrayList;
    Fonts fonts;

    public AllChatAdapter(Context c, List<Chat> arrayList, Fonts fonts) {
        mContext = c;
        this.arrayList = arrayList;
        this.fonts=fonts;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_chat_item, viewGroup, false);
        return new ViewHolder(view,fonts);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Chat chat=arrayList.get(i);
        if(chat.getSender()==0){
            viewHolder.name.setText(chat.getFrom().getName());
            viewHolder.name.setTextColor(Color.parseColor("#666666"));
        }else{
            viewHolder.name.setText(chat.getTo().getName());
            viewHolder.name.setTextColor(Color.parseColor("#999999"));
        }
        viewHolder.message.setText(chat.getBody());
        Glide.with(mContext).load(chat.getProduct().getImage().getPath()).into(viewHolder.image);
        try {
            viewHolder.date.setText(Common.parseInternational(chat.getCreatedAt()));
        } catch (ParseException ignored) {
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.message)
        TextView message;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.image)
        ImageView image;

        public ViewHolder(View view,Fonts fonts) {
            super(view);
            ButterKnife.bind(this,view);
            name.setTypeface(fonts.bold);
            message.setTypeface(fonts.small);
            date.setTypeface(fonts.small);

        }

    }


}
