package com.webaspiration.mechanics.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.webaspiration.mechanics.Dagger.Provides.Fonts;
import com.webaspiration.mechanics.Network.Beans.Product;
import com.webaspiration.mechanics.R;

import java.util.List;

/**
 * Created by darkapple on 27/11/17.
 */

public class SearchProductAdapter extends BaseAdapter {


    Fonts fonts;
    List<Product>products;

    public SearchProductAdapter(Fonts fonts, List<Product> products) {
        this.fonts = fonts;
        this.products = products;
    }


    public void setProducts(List<Product> products) {
        this.products = products;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Product product=products.get(i);
        if(view==null){
            LayoutInflater inflater=LayoutInflater.from(viewGroup.getContext());
            view=inflater.inflate(R.layout.adapter_search_product_item,viewGroup,false);
        }
        if(product.getProductImages().size()>0){
            Picasso.with(viewGroup.getContext()).load(product.getProductImages().get(0).getPath())
                    .into(((ImageView)view.findViewById(R.id.image)));
        }
        ((TextView)view.findViewById(R.id.name)).setTypeface(fonts.bold);
        ((TextView)view.findViewById(R.id.des)).setTypeface(fonts.small);
        ((TextView)view.findViewById(R.id.name)).setText(product.getName());
        ((TextView)view.findViewById(R.id.des)).setText(product.getDescription());
        return view;
    }
}
