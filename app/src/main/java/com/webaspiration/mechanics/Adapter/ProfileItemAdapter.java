package com.webaspiration.mechanics.Adapter;

import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.webaspiration.mechanics.R;
import java.util.ArrayList;

public class ProfileItemAdapter extends BaseAdapter {


    ArrayList<NavDrawerItem> arrayList;
    Typeface small;


    public ProfileItemAdapter(ArrayList<NavDrawerItem> arrayList, Typeface smalll) {
        this.arrayList = arrayList;
        this.small = smalll;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public NavDrawerItem getItem(int i) {
        return arrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.drawer_list_item, viewGroup, false);
        ((TextView) view.findViewById(R.id.title)).setText(getItem(i).getTitle());
        ((TextView) view.findViewById(R.id.title)).setTypeface(small);
        ((ImageView)view.findViewById(R.id.icon)).setImageDrawable(viewGroup.getContext().getResources().getDrawable(getItem(i).getIcon()));
        return view;
    }
}
