package com.webaspiration.mechanics.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.webaspiration.mechanics.Dagger.Provides.Fonts;
import com.webaspiration.mechanics.Network.Beans.Product;
import com.webaspiration.mechanics.R;

import java.util.List;


public class MyProductAdapter extends RecyclerView.Adapter<MyProductAdapter.ViewHolder> {

    private Context mContext;
    private List<Product> arrayList;
    Fonts fonts;

    public MyProductAdapter(Context c, List<Product> arrayList, Fonts fonts) {
        mContext = c;
        this.arrayList = arrayList;
        this.fonts=fonts;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.product_item, viewGroup, false);
        return new ViewHolder(view,fonts);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Product product=arrayList.get(i);
        viewHolder.name.setText(product.getName());
        viewHolder.des.setText(product.getDescription());
        viewHolder.mrp.setText(String.format("Rs %s",product.getPrice()));
        if(product.getProductImages().size()>0)
        Glide.with(mContext).load(product.getProductImages().get(0).getPath()).into(viewHolder.image);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private TextView mrp;
        private TextView des;
        private ImageView image;

        public ViewHolder(View view,Fonts fonts) {
            super(view);
            name = (TextView) view.findViewById(R.id.search_prod_name);
            image = (ImageView) view.findViewById(R.id.search_prod_image);
            mrp = (TextView) view.findViewById(R.id.search_prod_mrp);
            des = (TextView) view.findViewById(R.id.description);
            name.setTypeface(fonts.bold);
            mrp.setTypeface(fonts.small);
            des.setTypeface(fonts.small);
        }

    }


}
