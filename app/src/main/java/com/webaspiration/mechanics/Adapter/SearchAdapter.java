package com.webaspiration.mechanics.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.webaspiration.mechanics.R;

import java.util.List;

/**
 * Created by shaan on 10/12/2017.
 */

public class SearchAdapter extends BaseAdapter {

    List<String> list;

    public SearchAdapter(List<String> list){
        this.list=list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null)
            convertView= LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_list_item,parent,false);
        ((TextView)convertView.findViewById(R.id.title)).setText(list.get(position));
        return convertView;
    }
}
