
package com.webaspiration.mechanics.Network.Beans.ChatResponse;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class AllChatResponse {

    @SerializedName("chats")
    private List<Chat> mChats;

    public List<Chat> getChats() {
        return mChats;
    }

    public void setChats(List<Chat> chats) {
        mChats = chats;
    }

}
