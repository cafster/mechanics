
package com.webaspiration.mechanics.Network.Beans;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class CategoryResponse {

    @SerializedName("category")
    private List<Category> mCategory;

    public List<Category> getCategory() {
        return mCategory;
    }

    public void setCategory(List<Category> category) {
        mCategory = category;
    }

}
