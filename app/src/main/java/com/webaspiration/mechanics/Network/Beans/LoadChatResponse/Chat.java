
package com.webaspiration.mechanics.Network.Beans.LoadChatResponse;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Chat {

    @SerializedName("body")
    private String mBody;
    @SerializedName("createdAt")
    private String mCreatedAt;
    @SerializedName("seen")
    private Boolean mSeen;
    @SerializedName("user")
    private Long mUser;

    public String getBody() {
        return mBody;
    }

    public void setBody(String body) {
        mBody = body;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public Boolean getSeen() {
        return mSeen;
    }

    public void setSeen(Boolean seen) {
        mSeen = seen;
    }

    public Long getUser() {
        return mUser;
    }

    public void setUser(Long user) {
        mUser = user;
    }

}
