
package com.webaspiration.mechanics.Network.Beans.UnseenResponse;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Chat {

    @SerializedName("body")
    private String mBody;
    @SerializedName("createdAt")
    private String mCreatedAt;
    @SerializedName("id")
    private Long mId;
    @SerializedName("seen")
    private Boolean mSeen;

    public String getBody() {
        return mBody;
    }

    public void setBody(String body) {
        mBody = body;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Boolean getSeen() {
        return mSeen;
    }

    public void setSeen(Boolean seen) {
        mSeen = seen;
    }

}
