
package com.webaspiration.mechanics.Network.Beans.ChatResponse;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Chat {

    @SerializedName("body")
    private String mBody;
    @SerializedName("createdAt")
    private String mCreatedAt;
    @SerializedName("from")
    private From mFrom;
    @SerializedName("id")
    private Long mId;
    @SerializedName("product")
    private Product mProduct;
    @SerializedName("sender")
    private Long mSender;
    @SerializedName("to")
    private To mTo;

    public String getBody() {
        return mBody;
    }

    public void setBody(String body) {
        mBody = body;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public From getFrom() {
        return mFrom;
    }

    public void setFrom(From from) {
        mFrom = from;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Product getProduct() {
        return mProduct;
    }

    public void setProduct(Product product) {
        mProduct = product;
    }

    public Long getSender() {
        return mSender;
    }

    public void setSender(Long sender) {
        mSender = sender;
    }

    public To getTo() {
        return mTo;
    }

    public void setTo(To to) {
        mTo = to;
    }

}
