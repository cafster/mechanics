package com.webaspiration.mechanics.Network.Place;

import com.webaspiration.mechanics.Network.Place.Beans.PlacePrediction;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by shaan on 10/12/2017.
 */

public interface Place {

    @GET("place/autocomplete/json")
    Observable<Response<PlacePrediction>> predictions(@Query("key") String key, @Query("input") String input, @Query("types") String types);

    @GET("place/autocomplete/json")
    Observable<Response<PlacePrediction>> predictions(@Query("key") String key, @Query("input") String input);

    @GET("distancematrix/json?sensor=false&mode=driving")
    Observable<Response<com.webaspiration.mechanics.Network.Place.DirectionResponse.Response>> getDirections(@Query("origins") String origin,
                                                                                                             @Query("destinations") String destination, @Query("key") String mode);

}
