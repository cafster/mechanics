package com.webaspiration.mechanics.Network;

import com.webaspiration.mechanics.Network.Beans.CategoryResponse;
import com.webaspiration.mechanics.Network.Beans.ChatResponse.AllChatResponse;
import com.webaspiration.mechanics.Network.Beans.GetProductResponse;
import com.webaspiration.mechanics.Network.Beans.LoadChatResponse.LoadChatResponse;
import com.webaspiration.mechanics.Network.Beans.MeResponse.MeResponse;
import com.webaspiration.mechanics.Network.Beans.ProductPostResponse;
import com.webaspiration.mechanics.Network.Beans.ProductResponse;
import com.webaspiration.mechanics.Network.Beans.UnseenResponse.UnseenResponse;
import com.webaspiration.mechanics.Network.Beans.User;
import com.webaspiration.mechanics.Network.Model.Error;
import com.webaspiration.mechanics.Network.Model.Success;


import java.util.HashMap;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by darkapple on 05/11/17.
 */

public interface Network {


    @FormUrlEncoded
    @POST("login")
    Observable<Response<User>> login(@Field("mobile")String m,@Field("password")String p);
    @FormUrlEncoded
    @POST("login")
    Observable<Response<User>> login(@FieldMap HashMap<String,String>map);
    @FormUrlEncoded
    @POST("location")
    Observable<Response<Success>> location(@Field("lat") String lat,@Field("lng")String lng);

    @FormUrlEncoded
    @POST("signup")
    Observable<Response<Success>> signup(@FieldMap HashMap<String,String>map);

    @FormUrlEncoded
    @POST("password")
    Observable<Response<Success>> password(@Field("password")String password);
    @FormUrlEncoded
    @POST("fcm")
    Call<Success> fcm(@Field("fcm")String fcm);

    @FormUrlEncoded
    @POST("reset")
    Observable<Response<Error>> reset(@Field("number")String number);

    @GET("products")
    Observable<Response<ProductResponse>> products(@Query("lat") String lat,@Query("lng")String lng);
    @GET("products")
    Observable<Response<ProductResponse>> products(@Query("search") String search,@Query("lat") String lat,@Query("lng")String lng);

    @GET("products/mine/new")
    Observable<Response<ProductResponse>> newProducts();

    @GET("products/mine/old")
    Observable<Response<ProductResponse>> oldProducts();

    @GET("category")
    Observable<Response<CategoryResponse>> getCatges();

    @GET("me")
    Observable<Response<MeResponse>> me();

    @POST("product")
    @FormUrlEncoded
    Observable<Response<ProductPostResponse>> postProfuct(@FieldMap HashMap<String,String>map);

    @GET("product/{id}")
    Observable<Response<GetProductResponse>> getProduct(@Path("id")String id);

    @Multipart
    @POST("product/{id}")
    Call<Success> postImage(@Path("id") String id, @Part MultipartBody.Part image);

    @DELETE("product/{id}")
    Observable<Response<Success>> deleteProduct(@Path("id") String id);

    @GET("chats/all")
    Observable<Response<AllChatResponse>> getChats();

    @GET("chats/{id}/{sid}")
    Observable<Response<LoadChatResponse>> loadChat(@Path("id")String id, @Path("sid") String sid);

    @FormUrlEncoded
    @POST("chats/{id}/{sid}")
    Observable<Response<Success>> postMessage(@Path("id")String id, @Path("sid") String sid,@Field("body") String body);

    @GET("chats/{id}/{sid}/unseen")
    Observable<Response<UnseenResponse>> unseen(@Path("id")String id, @Path("sid") String sid);

}
