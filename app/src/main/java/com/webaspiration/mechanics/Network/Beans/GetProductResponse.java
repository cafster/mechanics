
package com.webaspiration.mechanics.Network.Beans;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class GetProductResponse {

    @SerializedName("isuser")
    private Boolean mIsuser;
    @SerializedName("product")
    private Product mProduct;

    public Boolean getIsuser() {
        return mIsuser;
    }

    public void setIsuser(Boolean isuser) {
        mIsuser = isuser;
    }

    public Product getProduct() {
        return mProduct;
    }

    public void setProduct(Product product) {
        mProduct = product;
    }

}
