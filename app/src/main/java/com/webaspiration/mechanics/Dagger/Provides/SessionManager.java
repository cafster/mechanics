package com.webaspiration.mechanics.Dagger.Provides;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;


import com.google.android.gms.maps.model.LatLng;
import com.webaspiration.mechanics.Dagger.Retention.ApplicationContext;
import com.webaspiration.mechanics.Network.Beans.User;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class SessionManager {


    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private static final String PREF_NAME = "__byte__";

    private String _token = "__token__";
    private String _name = "__name__";
    private String __mobile = "__mobile";
    private String __image = "__image";


    private String lat="__lat__";
    private String lng="__lng__";

    @Inject
    public SessionManager(@ApplicationContext Context context) {
        int PRIVATE_MODE = 0;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public boolean loggedin(){
        return !pref.getString(_token,"").equals("");
    }


    public String token() {
        return pref.getString(_token, "");
    }


    public String name() {
        return pref.getString(_name, "");
    }

    public String mobile() {
        return pref.getString(__mobile, "");
    }

    public void save(User user){
        editor.putString(_name,user.getName());
        editor.putString(_token,user.getToken());
        editor.putString(__mobile,user.getMobile());
        editor.putString(__image,user.getImage());
        editor.commit();
    }

    public String image() {
        return pref.getString(__image, "");
    }

    public void saveLocation(LatLng latLng){
        editor.putString(lat,String.valueOf(latLng.latitude));
        editor.putString(lng,String.valueOf(latLng.longitude));
        editor.commit();
    }

    public LatLng getLocation(){
        if(pref.getString(lat,"").equals(""))
            return null;
        double _lat=Double.parseDouble(pref.getString(lat,""));
        double _lng=Double.parseDouble(pref.getString(lng,""));
        return new LatLng(_lat,_lng);
    }

    public void logout() {
        editor.clear();
        editor.commit();
    }

    public void fcm(String fcm){
        editor.putString("fcm",fcm);
        editor.commit();
    }
    public boolean isfcm(){
        return !pref.getString("fcm","").equals("");
    }
    public String fcm(){
        return pref.getString("fcm","");
    }

}
