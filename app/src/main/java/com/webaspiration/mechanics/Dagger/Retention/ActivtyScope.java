package com.webaspiration.mechanics.Dagger.Retention;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by shaan on 7/25/2017.
 */

@SuppressWarnings("DefaultFileTemplate")
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ActivtyScope {
}
