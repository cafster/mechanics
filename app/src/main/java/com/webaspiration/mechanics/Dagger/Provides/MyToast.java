package com.webaspiration.mechanics.Dagger.Provides;

import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


import com.webaspiration.mechanics.Dagger.Retention.ApplicationContext;
import com.webaspiration.mechanics.Interface.Click;
import com.webaspiration.mechanics.R;

import java.lang.ref.WeakReference;

import javax.inject.Inject;
import javax.inject.Singleton;


@SuppressWarnings("DefaultFileTemplate")
@Singleton
public class MyToast {


    WeakReference<Context> context;
    String message;
    CoordinatorLayout coordinatorLayout;
    public Snackbar snackbar=null;
    public Fonts fonts;


    @Inject
    public MyToast(@ApplicationContext Context context, Fonts fonts){
        this.context=new WeakReference<Context>(context);
        this.fonts=fonts;
    }



    public MyToast message(String message) {
        this.message = message;
        return this;
    }

    public MyToast on(CoordinatorLayout cd) {
        coordinatorLayout = cd;
        return this;
    }

    public Snackbar show(){
        snackbar.show();
        return snackbar;
    }

    public Snackbar show(String name, final Click click) {
        snackbar.setAction(name, v -> click.onclick());
        snackbar.setActionTextColor(Color.WHITE);
        snackbar.setDuration(BaseTransientBottomBar.LENGTH_INDEFINITE);
        snackbar.show();
        return snackbar;
    }


    public void toast() {
        Context mContext=this.context.get();
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View layout = inflater.inflate(R.layout.toast,null);
        TextView text = (TextView) layout.findViewById(R.id.message);
        text.setText(message);
        text.setTypeface(fonts.small);
        Toast toast = new Toast(mContext);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

    public MyToast snackbar() {
        Context mContext=this.context.get();
        Snackbar snackbar = Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_LONG);
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(mContext, R.color.toast));
        TextView textView = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        textView.setTypeface(fonts.bold);
        this.snackbar = snackbar;
        return this;
    }

}
