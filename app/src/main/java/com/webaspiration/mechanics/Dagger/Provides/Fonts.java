package com.webaspiration.mechanics.Dagger.Provides;

import android.content.Context;
import android.graphics.Typeface;

import com.webaspiration.mechanics.Dagger.Retention.ApplicationContext;

import javax.inject.Inject;
import javax.inject.Singleton;


@Singleton
public class Fonts {


    public Typeface bold;
    public Typeface small;
    public Typeface title;
    public Typeface title2;

    @Inject
    public Fonts(@ApplicationContext Context context){
        bold=Typeface.createFromAsset(context.getAssets(),"fonts/fontsb.ttf");
        small=Typeface.createFromAsset(context.getAssets(),"fonts/fontsr.ttf");
        title=Typeface.createFromAsset(context.getAssets(),"fonts/fontsb.ttf");
        title2=Typeface.createFromAsset(context.getAssets(),"fonts/fontsb.ttf");
    }

}
