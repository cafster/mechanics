package com.webaspiration.mechanics.Dagger.Component;


import com.webaspiration.mechanics.Dagger.Module.ActivityModule;
import com.webaspiration.mechanics.Dagger.Retention.ActivtyScope;
import com.webaspiration.mechanics.Ui.DetectLocation.DetectLocation;
import com.webaspiration.mechanics.Ui.Home.Home;
import com.webaspiration.mechanics.Ui.LocationSearch.LocationComponent;
import com.webaspiration.mechanics.Ui.LocationSearch.LocationModule;
import com.webaspiration.mechanics.Ui.Splash.Module;

import dagger.Component;


@SuppressWarnings("DefaultFileTemplate")
@ActivtyScope
@Component(dependencies = ApplicationComponent.class,modules = ActivityModule.class)
public interface ActivityComponent {
    com.webaspiration.mechanics.Ui.Search.Component plus(com.webaspiration.mechanics.Ui.Search.Module m);
    com.webaspiration.mechanics.Ui.ForgetPassword.Component plus(com.webaspiration.mechanics.Ui.ForgetPassword.Module m);
    com.webaspiration.mechanics.Ui.ChangePassword.Component plus(com.webaspiration.mechanics.Ui.ChangePassword.Module m);
    com.webaspiration.mechanics.Ui.Splash.Component plus(Module m);
    com.webaspiration.mechanics.Ui.Login.Component plus(com.webaspiration.mechanics.Ui.Login.Module m);
    com.webaspiration.mechanics.Ui.Signup.Component plus(com.webaspiration.mechanics.Ui.Signup.Module m);
    void inject(Home activity);
    com.webaspiration.mechanics.Ui.Chat.Component plus(com.webaspiration.mechanics.Ui.Chat.Module m);
    void inject(DetectLocation ac);
    com.webaspiration.mechanics.Ui.Settings.Component plus(com.webaspiration.mechanics.Ui.Settings.Module m);
    LocationComponent plus(LocationModule m);
    com.webaspiration.mechanics.Ui.ViewProduct.Component plus(com.webaspiration.mechanics.Ui.ViewProduct.Module m);
    com.webaspiration.mechanics.Ui.PostProduct.Component plus(com.webaspiration.mechanics.Ui.PostProduct.Module m);
    com.webaspiration.mechanics.Ui.Home.Main.Component plus(com.webaspiration.mechanics.Ui.Home.Main.Module m);
    com.webaspiration.mechanics.Ui.Home.Chat.Component plus(com.webaspiration.mechanics.Ui.Home.Chat.Module m);
    com.webaspiration.mechanics.Ui.Home.MyAds.Component plus(com.webaspiration.mechanics.Ui.Home.MyAds.Module m);
    com.webaspiration.mechanics.Ui.Home.Profile.Component plus(com.webaspiration.mechanics.Ui.Home.Profile.Module m);
    com.webaspiration.mechanics.Ui.Home.MyAds.NewProducts.Component plus(com.webaspiration.mechanics.Ui.Home.MyAds.NewProducts.Module n);
    com.webaspiration.mechanics.Ui.Home.MyAds.OldProducts.Component plus(com.webaspiration.mechanics.Ui.Home.MyAds.OldProducts.Module m);

}
