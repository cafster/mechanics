package com.webaspiration.mechanics.Dagger.Component;

import android.content.Context;

import com.webaspiration.mechanics.Dagger.Module.ApplicationModule;
import com.webaspiration.mechanics.Dagger.Provides.Client;
import com.webaspiration.mechanics.Dagger.Provides.Common;
import com.webaspiration.mechanics.Dagger.Provides.Fonts;
import com.webaspiration.mechanics.Dagger.Provides.MyToast;
import com.webaspiration.mechanics.Dagger.Provides.SessionManager;
import com.webaspiration.mechanics.Dagger.Retention.ApplicationContext;
import com.webaspiration.mechanics.View.MyApplication;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by shaan on 8/31/2017.
 */


@SuppressWarnings("DefaultFileTemplate")
@Singleton
@Component(modules= ApplicationModule.class)
public interface ApplicationComponent {

    void inject(MyApplication demoApplication);

    @ApplicationContext
    Context getContext();

    MyToast toast();
    SessionManager sess();
    Client client();
    Fonts fonts();

}