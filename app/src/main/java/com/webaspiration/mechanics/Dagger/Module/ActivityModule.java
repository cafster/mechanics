package com.webaspiration.mechanics.Dagger.Module;

import android.app.Activity;
import android.content.Context;


import com.webaspiration.mechanics.Dagger.Retention.ActivityContext;

import dagger.Module;
import dagger.Provides;

/**
 * Created by shaan on 8/31/2017.
 */

@SuppressWarnings("DefaultFileTemplate")
@Module
public class ActivityModule {

    private Activity mActivity;

    public ActivityModule(Activity activity) {
        mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    Activity provideActivity() {
        return mActivity;
    }
}