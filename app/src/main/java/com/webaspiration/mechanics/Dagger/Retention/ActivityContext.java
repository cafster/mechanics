package com.webaspiration.mechanics.Dagger.Retention;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

/**
 * Created by shaan on 8/31/2017.
 */

@SuppressWarnings("DefaultFileTemplate")
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
public @interface ActivityContext {
}
