package com.webaspiration.mechanics.Dagger.Provides;


import android.content.Context;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.google.gson.Gson;
import com.webaspiration.mechanics.Dagger.Retention.ApplicationContext;

import javax.inject.Inject;
import javax.inject.Singleton;


public class Common {

    private static Gson gson;

    public static Gson getGson() {
        if (gson==null)
            gson=new Gson();
        return gson;
    }

}
