package com.webaspiration.mechanics.Dagger.Provides;

import android.content.Context;

import com.webaspiration.mechanics.Common.Constants;
import com.webaspiration.mechanics.Dagger.Retention.ApplicationContext;
import com.webaspiration.mechanics.Network.Network;
import com.webaspiration.mechanics.Network.Place.Place;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@SuppressWarnings("DefaultFileTemplate")
@Singleton
public class Client {


    public static OkHttpClient client;
    public static Network mClient;

    private static Place place;
    public static Place place() {
        if (place == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.googleUrl)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            place = retrofit.create(Place.class);
        }
        return place;
    }



    @Inject
    Client(@ApplicationContext Context c, SessionManager sessionManager) {
        if(mClient==null){
            File httpCacheDirectory = new File(c.getCacheDir(), "responses");
            int cacheSize = 40 * 1024 * 1024;
            Cache cache = new Cache(httpCacheDirectory, cacheSize);
            client = new OkHttpClient.Builder()
                    .cache(cache)
                    .addInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            Request request = chain.request();
                            Request newRequest;
                            newRequest = request.newBuilder()
                                    .addHeader("Cache-Control", "public,max-age=600")
                                    .addHeader("Authorization", String.format(sessionManager.token()))
                                    .build();
                            return chain.proceed(newRequest);
                        }
                    })
                    .writeTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .build();
            Retrofit retrofit=new Retrofit.Builder().baseUrl(Constants.apiUrl)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(Common.getGson()))
                    .client(client).build();
            mClient=retrofit.create(Network.class);
        }
    }

}
