package com.webaspiration.mechanics.View;

import android.app.Application;

import com.google.firebase.FirebaseApp;
import com.webaspiration.mechanics.Dagger.Component.ApplicationComponent;
import com.webaspiration.mechanics.Dagger.Component.DaggerApplicationComponent;
import com.webaspiration.mechanics.Dagger.Module.ApplicationModule;


@SuppressWarnings("DefaultFileTemplate")
public class MyApplication extends Application {

    public static ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        if(component==null){
            component= DaggerApplicationComponent.builder().applicationModule(new ApplicationModule(this)).build();
        }
        component.inject(this);
        try{
            FirebaseApp.initializeApp(this);
        }catch (Exception ignored){

        }
    }
}