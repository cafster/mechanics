package com.webaspiration.mechanics.View.message;

import android.graphics.Bitmap;

/**
 * User object
 * Created by nakayama on 2017/01/12.
 */
public class User {
    private int mId;
    private String mName;
    private Bitmap mIcon;
    private String path;

    public User(int id, String name, String path) {
        mId = id;
        mName = name;
        this.path=path;
    }

    public String getPath(){
        return  path;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public Bitmap getIcon() {
        return mIcon;
    }

    public void setIcon(Bitmap icon) {
        mIcon = icon;
    }
}
