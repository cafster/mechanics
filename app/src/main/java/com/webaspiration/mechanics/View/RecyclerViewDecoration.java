package com.webaspiration.mechanics.View;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by lakshay on 9/7/17.
 */

@SuppressWarnings("DefaultFileTemplate")
public class RecyclerViewDecoration extends RecyclerView.ItemDecoration {
    private int space;

    public RecyclerViewDecoration(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

        if(parent.getChildAdapterPosition(view) == state.getItemCount()-1){
            outRect.bottom = space;
        }
    }
}