package com.webaspiration.mechanics.Service;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.webaspiration.mechanics.Dagger.Provides.Client;
import com.webaspiration.mechanics.Dagger.Provides.SessionManager;
import com.webaspiration.mechanics.Network.Model.Success;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by darkapple on 27/11/17.
 */

public class FirebaseInstanceService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("TOKEN REFRESHED",refreshedToken);
        SessionManager sessionManager=new SessionManager(getApplicationContext());
        if(sessionManager.loggedin()){
            Client.mClient.fcm(refreshedToken).enqueue(new Callback<Success>() {
                @Override
                public void onResponse(Call<Success> call, Response<Success> response) {
                    Log.d("Refresh Token","Posted");
                }

                @Override
                public void onFailure(Call<Success> call, Throwable t) {

                }
            });
        }
        sessionManager.fcm(refreshedToken);
    }
}
