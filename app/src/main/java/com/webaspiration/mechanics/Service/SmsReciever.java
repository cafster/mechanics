package com.webaspiration.mechanics.Service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;


import com.webaspiration.mechanics.Events.SmsRecievedEvent;

import org.greenrobot.eventbus.EventBus;

import static android.provider.Telephony.Sms.Intents.SMS_RECEIVED_ACTION;

/**
 * Created by lakshay on 28/7/17.
 */

@SuppressWarnings("DefaultFileTemplate")
public class SmsReciever extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("SmS","Registered");
        if (intent.getAction().equals(SMS_RECEIVED_ACTION)) {
            Bundle extras = intent.getExtras();

            String strMessage = "";

            if (extras != null) {
                Object[] smsextras = (Object[]) extras.get("pdus");

                for (int i = 0; i < smsextras.length; i++) {
                    SmsMessage smsmsg = SmsMessage.createFromPdu((byte[]) smsextras[i]);

                    String strMsgBody = smsmsg.getMessageBody().toString();
                    String strMsgSrc = smsmsg.getOriginatingAddress();

                    strMessage += strMsgBody;

                }

                Log.d("SmS","Recieved");
                strMessage = strMessage.toLowerCase();
                if (strMessage.contains("Machine Becho")) {
                    Log.d("SmS","Parsed");
                    String otpSms = "";
                    try {
                        int length = new String("Your verification code is: ").length();
                        otpSms = strMessage.substring(length, length + 4);
                        Log.d("SmS",otpSms);
                        EventBus.getDefault().post(new SmsRecievedEvent(otpSms));
                    } catch (Exception e) {
                        return;
                    }

                }
            }
        }

    }
}
