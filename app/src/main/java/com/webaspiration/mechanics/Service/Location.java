package com.webaspiration.mechanics.Service;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;


import com.webaspiration.mechanics.Events.LocationEvent;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by lakshay on 26/7/17.
 */

public class Location extends Service {


    private static final int TWO_MINUTES = 0;
    private static final int DISTANCE = 0;
    public LocationManager locationManager;
    public MyLocationListener listener;


    @Override
    public void onCreate() {
        super.onCreate();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        listener = new MyLocationListener();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, TWO_MINUTES, DISTANCE, listener);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, TWO_MINUTES, DISTANCE, listener);
        locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, TWO_MINUTES, DISTANCE, listener);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_NOT_STICKY;
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        locationManager.removeUpdates(listener);
    }


    public class MyLocationListener implements LocationListener {

        public void onLocationChanged(final android.location.Location loc) {
            EventBus.getDefault().postSticky(new LocationEvent(loc));
        }

        public void onProviderDisabled(String provider) {
        }


        public void onProviderEnabled(String provider) {
        }


        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

    }
}
