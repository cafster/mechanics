package com.webaspiration.mechanics.Ui.Signup;

import com.webaspiration.mechanics.Common.Validates;
import com.webaspiration.mechanics.Dagger.Provides.Client;
import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Model.Success;
import com.webaspiration.mechanics.Network.Network;

import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by darkapple on 05/11/17.
 */

public class Controller implements MVP.Model {

    Network api;
    CompositeDisposable disposable;

    public Controller(Client api) {
        this.api = api.mClient;
        disposable=new CompositeDisposable();
    }

    @Override
    public void signUp(HashMap<String, String> map, NetworkInteractor<Success> handler) {
        disposable.add(api.signup(map).flatMap(new Validates()).subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread()).subscribe(handler::handle,handler::error));
    }

    @Override
    public void destroy() {
        if(!disposable.isDisposed())
            disposable.dispose();
    }
}
