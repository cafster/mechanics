package com.webaspiration.mechanics.Ui.Login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.Task;
import com.webaspiration.mechanics.Dagger.Provides.Common;
import com.webaspiration.mechanics.Dagger.Provides.Fonts;
import com.webaspiration.mechanics.R;
import com.webaspiration.mechanics.Ui.Base.BaseActivity;
import com.webaspiration.mechanics.Ui.DetectLocation.DetectLocation;
import com.webaspiration.mechanics.Ui.ForgetPassword.ForgotPassword;
import com.webaspiration.mechanics.Ui.Home.Home;
import com.webaspiration.mechanics.Ui.Signup.Signup;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import javax.inject.Inject;

import butterknife.BindView;

public class LoginActivity extends BaseActivity implements MVP.View,
        GoogleApiClient.OnConnectionFailedListener {


    @BindView(R.id.login_bottom_image)
    ImageView login_bottom_image;

    @BindView(R.id.login_button)
    LoginButton loginButton;

    @Inject
    Fonts fonts;

    @BindView(R.id.login_password)
    EditText login_password;

    @BindView(R.id.login_name)
    EditText login_name;

    @BindView(R.id.login_attemptlogin)
    Button login_attemptlogin;

    @BindView(R.id.forgotpassword)
    TextView forgotpassword;

    @BindView(R.id.login_signup)
    TextView login_signup;

    @Override
    public String name() {
        return login_name.getText().toString();
    }

    @Override
    public String password() {
        return login_password.getText().toString();
    }

    @Inject
    MVP.Presentor presentor;

    private CallbackManager callbackManager;


    private GoogleSignInClient mGoogleSignInClient;

    @Override
    public void facebook() {
        super.facebook();
        FacebookSdk.sdkInitialize(getApplicationContext());
    }


    @Override
    public void initialize() {
        presentor.setView(this);
        overridePendingTransition(R.anim.in_from_right,R.anim.out_to_left);
        Glide.with(this).load(R.drawable.login_bottom_background).into(login_bottom_image);
        ((TextView)findViewById(R.id.login_title)).setTypeface(fonts.bold);
        ((TextView)findViewById(R.id.or)).setTypeface(fonts.bold);
        ((TextView)findViewById(R.id.forgotpasswordtitle)).setTypeface(fonts.small);
        ((Button)findViewById(R.id.login_fbbutton)).setTypeface(fonts.small);
        ((Button)findViewById(R.id.login_gogolebutton)).setTypeface(fonts.small);
        forgotpassword.setTypeface(fonts.bold);
        login_attemptlogin.setTypeface(fonts.small);
        login_name.setTypeface(fonts.small);
        login_password.setTypeface(fonts.small);
        forgotpassword.setOnClickListener(view -> startActivity(new Intent(this, ForgotPassword.class)));
        login_signup.setTypeface(fonts.bold);
        ((TextView)findViewById(R.id.login_signup_des)).setTypeface(fonts.small);
        login_signup.setOnClickListener(view -> {
            startActivity(new Intent(this, Signup.class));
            finish();
        });
        login_attemptlogin.setOnClickListener(view -> presentor.login());
        loginButton.setReadPermissions("public_profile");
        loginButton.setReadPermissions("email");
        ((Button)findViewById(R.id.login_fbbutton)).setOnClickListener(view -> loginButton.performClick());
        callbackManager = CallbackManager.Factory.create();
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                try {
                                    HashMap<String,String>data=new HashMap<>();
                                    data.put("name",object.getString("name"));
                                    data.put("id",object.getString("id"));
                                    data.put("email",object.getString("email"));
                                    data.put("image",object.getJSONObject("picture").getJSONObject("data").getString("url"));
                                    HashMap<String,String>map=new HashMap<>();
                                    map.put("type","Fb");
                                    map.put("data",Common.getGson().toJson(data));
                                    presentor.login(map);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                LoginManager.getInstance().logOut();
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,picture.type(large)");
                request.setParameters(parameters);
                request.executeAsync();

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException e) {

            }
        });
        ((Button)findViewById(R.id.login_gogolebutton)).setOnClickListener(view -> {
            signIn();
        });
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }


    private static final int RC_SIGN_IN = 007;
    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            HashMap<String,String>data=new HashMap<>();
            data.put("name",account.getDisplayName());
            data.put("id",account.getId());
            data.put("email",account.getEmail());
            data.put("image",account.getPhotoUrl().toString());
            HashMap<String,String>map=new HashMap<>();
            map.put("type","Google");
            map.put("data",Common.getGson().toJson(data));
            presentor.login(map);
        } catch (ApiException e) {
        }
    }

    @Override
    public int layout() {
        return R.layout.activity_login;
    }

    @Override
    public void dependency() {
        component().plus(new Module()).inject(this);
    }

    @Override
    public void networkcall() {

    }

    @Override
    public void message(String message) {
        if (toast.snackbar != null && toast.snackbar.isShownOrQueued()) {
            toast.snackbar.dismiss();
        }
        if (coordinatorLayout != null) {
            toast.message(message).on(coordinatorLayout).snackbar().show();
        } else {
            toast.message(message).toast();
        }
    }

    @Override
    public void sendtohome() {
        startActivity(new Intent(this, DetectLocation.class));
        finish();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
