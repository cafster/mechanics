package com.webaspiration.mechanics.Ui.Home.Main;

import com.webaspiration.mechanics.Common.Validates;
import com.webaspiration.mechanics.Dagger.Provides.SessionManager;
import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Beans.ProductResponse;
import com.webaspiration.mechanics.Network.Network;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by darkapple on 09/11/17.
 */

public class Controller implements MVP.Model{

    Network mNetwork;
    CompositeDisposable disposable;
    SessionManager sessionManager;

    public Controller(Network mNetwork,SessionManager sessionManager) {
        this.mNetwork = mNetwork;
        this.sessionManager=sessionManager;
        disposable=new CompositeDisposable();
    }

    @Override
    public void destroy() {
        if(!disposable.isDisposed())
            disposable.dispose();
    }

    @Override
    public void clear() {
        disposable.clear();
    }

    @Override
    public void load(NetworkInteractor<ProductResponse> handler) {
        disposable.add(mNetwork.products(String.valueOf(sessionManager.getLocation().latitude),String.valueOf(sessionManager.getLocation().longitude))
        .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).flatMap(new Validates()).subscribe(handler::handle,handler::error));
    }
}
