package com.webaspiration.mechanics.Ui.Home.Profile;

/**
 * Created by darkapple on 09/11/17.
 */

public interface MVP {

    interface View{
        void message(String message);
    }

    interface Presentor{
        void destroy();
        void init();
        void setView(View c);
    }

    interface Model{
        void destroy();
    }

}
