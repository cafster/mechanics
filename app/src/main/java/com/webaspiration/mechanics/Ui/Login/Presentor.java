package com.webaspiration.mechanics.Ui.Login;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.webaspiration.mechanics.Dagger.Provides.Client;
import com.webaspiration.mechanics.Dagger.Provides.SessionManager;
import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Beans.User;
import com.webaspiration.mechanics.Network.Model.Success;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by darkapple on 05/11/17.
 */

public class Presentor implements MVP.Presentor{

    MVP.View view;
    MVP.Model model;
    SessionManager sessionManager;


    public Presentor(MVP.Model model, SessionManager sessionManager) {
        this.model = model;
        this.sessionManager = sessionManager;
    }

    @Override
    public void setView(MVP.View c) {
        this.view=c;
    }

    @Override
    public void destroy() {
        model.destroy();
    }

    @Override
    public void login(HashMap<String, String> map) {
        model.login(map, new NetworkInteractor<User>() {
            @Override
            public void handle(Response<User> o) {
                if(o.isSuccessful()){
                    User user=o.body();
                    sessionManager.save(user);
                    if(FirebaseInstanceId.getInstance().getToken()!=null){
                        Log.d("token",FirebaseInstanceId.getInstance().getToken());
                        Client.mClient.fcm(FirebaseInstanceId.getInstance().getToken()).enqueue(new Callback<Success>() {
                            @Override
                            public void onResponse(Call<Success> call, Response<Success> response) {

                            }

                            @Override
                            public void onFailure(Call<Success> call, Throwable t) {

                            }
                        });
                    }
                    view.sendtohome();
                }
            }

            @Override
            public void error(Throwable e) {
                view.message(e.getMessage());
            }
        });
    }

    @Override
    public void login() {
        if(view.name().length()<9 || view.password().length()<4){
            view.message("Please enter a valid input");
            return;
        }
        model.login(view.name(), view.password(), new NetworkInteractor<User>() {
            @Override
            public void handle(Response<User> o) {
                if(o.isSuccessful()){
                    User user=o.body();
                    sessionManager.save(user);
                    view.sendtohome();
                }
            }

            @Override
            public void error(Throwable e) {
                view.message(e.getMessage());
            }
        });
    }
}
