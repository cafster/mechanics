package com.webaspiration.mechanics.Ui.DetectLocation;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.webaspiration.mechanics.Common.Common;
import com.webaspiration.mechanics.Dagger.Provides.Client;
import com.webaspiration.mechanics.Dagger.Provides.Fonts;
import com.webaspiration.mechanics.Dagger.Provides.SessionManager;
import com.webaspiration.mechanics.Events.LocationEvent;
import com.webaspiration.mechanics.R;
import com.webaspiration.mechanics.Service.Location;
import com.webaspiration.mechanics.Ui.Base.BaseActivity;
import com.webaspiration.mechanics.Ui.Home.Home;
import com.webaspiration.mechanics.Ui.LocationSearch.Search;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class DetectLocation extends BaseActivity {



    boolean isLocation=false;

    @Inject
    Fonts fonts;

    @Inject
    SessionManager sessionManager;

    @Override
    public void initialize() {
        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
        ((TextView)findViewById(R.id.detectingLocation)).setTypeface(fonts.bold);
        ((TextView)findViewById(R.id.detectingDes)).setTypeface(fonts.small);
        ((TextView)findViewById(R.id.or)).setTypeface(fonts.bold);
        ((TextView)findViewById(R.id.selectLocation)).setTypeface(fonts.bold);
        findViewById(R.id.selectLocation).setOnClickListener(view -> {
            startActivityForResult(new Intent(this, Search.class),LocationRequest);
        });
    }

    @Override
    public int layout() {
        return R.layout.activity_detect_location;
    }

    @Override
    public void dependency() {
        component().inject(this);
    }

    @Override
    public void networkcall() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        startLocation();
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onLocationChangeEvent(LocationEvent event) {
        stopLocation();
        Client.mClient.location(String.valueOf(event.getLocation().getLatitude()),String.valueOf(event.getLocation().getLongitude()))
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(successResponse -> {
            sessionManager.saveLocation(new LatLng(event.getLocation().getLatitude(),event.getLocation().getLongitude()));
            startActivity(new Intent(this, Home.class));
            finish();
        },Throwable::printStackTrace);
    }


    public void startLocation() {
        Common.gpsOn(this);
        DetectLocationPermissionsDispatcher.requestlocationWithCheck(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopLocation();
    }

    void stopLocation(){
        if(isLocation)
            stopService(new Intent(this, Location.class));
        isLocation=false;
    }
    @NeedsPermission({Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
    void requestlocation() {
        startService(new Intent(this, Location.class));
        isLocation = true;
    }

    @OnShowRationale({Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
    void showRationaleForLocation(final PermissionRequest request) {
        new AlertDialog.Builder(this)
                .setMessage(R.string.permission_camera_rationale)
                .setPositiveButton(R.string.button_allow, (dialog, button) -> request.proceed())
                .setNegativeButton(R.string.button_deny, (dialog, button) -> request.cancel())
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        DetectLocationPermissionsDispatcher.onRequestPermissionsResult(this,requestCode,grantResults);
    }


    @OnPermissionDenied({Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
    void showLocationDeniedLocation() {
        Toast.makeText(this, R.string.permission_camera_rationale, Toast.LENGTH_SHORT).show();
    }


    int LocationRequest = 201;
    String location;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LocationRequest && resultCode == Activity.RESULT_OK) {
            location = data.getStringExtra("location");
            LatLng destination=Common.getLocationFromAddress(this,location);
            Client.mClient.location(String.valueOf(destination.latitude),String.valueOf(destination.longitude))
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(successResponse -> {
                sessionManager.saveLocation(destination);
                startActivity(new Intent(this, Home.class));
                finish();
            },Throwable::printStackTrace);

        }
    }
}
