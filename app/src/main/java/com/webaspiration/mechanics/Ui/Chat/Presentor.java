package com.webaspiration.mechanics.Ui.Chat;


import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Beans.LoadChatResponse.LoadChatResponse;
import com.webaspiration.mechanics.Network.Beans.UnseenResponse.UnseenResponse;
import com.webaspiration.mechanics.Network.Model.Success;

import retrofit2.Response;

/**
 * Created by darkapple on 16/11/17.
 */

public class Presentor implements MVP.Presentor {

    MVP.Model model;
    MVP.View view;

    public Presentor(MVP.Model m) {
        this.model = m;
    }

    @Override
    public void destroy() {
        model.destroy();
    }

    @Override
    public void init() {
        model.load(view.id(), view.uid(), new NetworkInteractor<LoadChatResponse>() {
            @Override
            public void handle(Response<LoadChatResponse> o) {
                if(o.isSuccessful())
                    view.load(o.body());
            }

            @Override
            public void error(Throwable e) {
                e.printStackTrace();
                view.message(e.getMessage());
            }
        });
    }

    @Override
    public void postMessage(String a) {
        model.post(view.id(), view.uid(), a, new NetworkInteractor<Success>() {
            @Override
            public void handle(Response<Success> o) {

            }

            @Override
            public void error(Throwable e) {
                view.message(e.getMessage());
            }
        });
    }

    @Override
    public void reload() {
        model.unseen(view.id(), view.uid(), new NetworkInteractor<UnseenResponse>() {
            @Override
            public void handle(Response<UnseenResponse> o) {
                if(o.isSuccessful())
                    view.load(o.body());
            }

            @Override
            public void error(Throwable e) {
                view.message(e.getMessage());
            }
        });
    }

    @Override
    public void setView(MVP.View c) {
        this.view = c;
    }

}
