package com.webaspiration.mechanics.Ui.Home.Profile;


import com.webaspiration.mechanics.Dagger.Retention.PerActivity;
import com.webaspiration.mechanics.Ui.Home.Main.Main;

import dagger.Subcomponent;

@SuppressWarnings("DefaultFileTemplate")
@PerActivity
@Subcomponent(modules = Module.class)
public interface Component {
    void inject(Profile fragment);
}