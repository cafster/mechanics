package com.webaspiration.mechanics.Ui.Home.Profile;

import com.webaspiration.mechanics.Network.Network;

import io.reactivex.disposables.CompositeDisposable;

public class Controller implements MVP.Model{

    Network mNetwork;
    CompositeDisposable disposable;

    public Controller(Network mNetwork) {
        this.mNetwork = mNetwork;
        disposable=new CompositeDisposable();
    }

    @Override
    public void destroy() {
        if(!disposable.isDisposed())
            disposable.dispose();
    }
}
