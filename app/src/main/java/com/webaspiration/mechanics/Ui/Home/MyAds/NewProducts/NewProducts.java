package com.webaspiration.mechanics.Ui.Home.MyAds.NewProducts;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.webaspiration.mechanics.Adapter.MyProductAdapter;
import com.webaspiration.mechanics.Dagger.Provides.Fonts;
import com.webaspiration.mechanics.Dagger.Retention.ActivityContext;
import com.webaspiration.mechanics.Network.Beans.Product;
import com.webaspiration.mechanics.Network.Beans.ProductResponse;
import com.webaspiration.mechanics.R;
import com.webaspiration.mechanics.Ui.Base.BaseFragment;
import com.webaspiration.mechanics.Ui.PostProduct.PostProduct;
import com.webaspiration.mechanics.Ui.ViewProduct.ViewProduct;
import com.webaspiration.mechanics.View.RecyclerItemClickListener;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewProducts extends BaseFragment implements MVP.View{


    com.webaspiration.mechanics.Ui.Home.MVP.View mvp;

    @Inject
    MVP.Presentor presentor;

    @Override
    public void message(String message) {
        mvp.message(message);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mvp=(com.webaspiration.mechanics.Ui.Home.MVP.View)getActivity();
        mvp.comp().plus(new Module()).inject(this);
        presentor.setView(this);
    }

    @Override
    protected void onconnected() {
        presentor.init();
    }

    @BindView(R.id.noorders)
    LinearLayout myProductsLayout;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @Inject
    Fonts fonts;

    @Override
    protected void oncreateview(View v) {
        ((TextView)v.findViewById(R.id.noorderstitle)).setTypeface(fonts.bold);
        ((TextView)v.findViewById(R.id.noordersdes)).setTypeface(fonts.small);
        ((TextView)v.findViewById(R.id.button)).setTypeface(fonts.small);
        (v.findViewById(R.id.button)).setOnClickListener(view -> {
            startActivity(new Intent(c, PostProduct.class));
        });
    }

    @Override
    protected int layout() {
        return R.layout.fragment_new_products;
    }

    @Inject
    @ActivityContext
    Context c;

    @Override
    public void load(ProductResponse response) {
        if(response.getProducts().size()>0){
            myProductsLayout.setVisibility(View.GONE);
            recyclerview.setVisibility(View.VISIBLE);
            recyclerview.setLayoutManager(new LinearLayoutManager(c));
            recyclerview.setAdapter(new MyProductAdapter(c,response.getProducts(),fonts));
            recyclerview.addOnItemTouchListener(new RecyclerItemClickListener(c,(view1, position) -> {
                Product product=response.getProducts().get(position);
                Intent i = new Intent(c, ViewProduct.class);
                i.putExtra("product", com.webaspiration.mechanics.Dagger.Provides.Common.getGson().toJson(product));
                startActivity(i);
            }));
        }else{
            myProductsLayout.setVisibility(View.VISIBLE);
            recyclerview.setVisibility(View.GONE);
        }
    }
}
