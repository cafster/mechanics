package com.webaspiration.mechanics.Ui.Splash;

/**
 * Created by darkapple on 05/11/17.
 */

public interface MVP {

    interface View{
        void message(String message);
        void sendToLogin();
        void sendToHome();
        void sendToLocation();
    }

    interface Presentor{
        void setView(View c);
        void init();
    }

}
