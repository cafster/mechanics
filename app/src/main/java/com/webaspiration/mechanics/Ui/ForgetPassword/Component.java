package com.webaspiration.mechanics.Ui.ForgetPassword;

import com.webaspiration.mechanics.Dagger.Retention.PerActivity;

import dagger.Subcomponent;

@SuppressWarnings("DefaultFileTemplate")
@PerActivity
@Subcomponent(modules = Module.class)
public interface Component {
    void inject(ForgotPassword login);
}