package com.webaspiration.mechanics.Ui.Splash;


import android.content.Intent;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.webaspiration.mechanics.Dagger.Provides.Client;
import com.webaspiration.mechanics.R;
import com.webaspiration.mechanics.Ui.Base.BaseActivity;
import com.webaspiration.mechanics.Ui.DetectLocation.DetectLocation;
import com.webaspiration.mechanics.Ui.Home.Home;
import com.webaspiration.mechanics.Ui.Login.LoginActivity;

import javax.inject.Inject;

public class Splash extends BaseActivity implements MVP.View{


    @Inject
    MVP.Presentor presentor;

    @Inject
    Client client;

    @Override
    public void message(String message) {
        if (toast.snackbar != null && toast.snackbar.isShownOrQueued()) {
            toast.snackbar.dismiss();
        }
        if (coordinatorLayout != null) {
            toast.message(message).on(coordinatorLayout).snackbar().show();
        } else {
            toast.message(message).toast();
        }
    }

    @Override
    public void initialize() {
        presentor.setView(this);
        presentor.init();
    }

    @Override
    public int layout() {
        return R.layout.activity_splash;
    }

    @Override
    public void dependency() {
        component().plus(new Module()).inject(this);
    }

    @Override
    public void networkcall() {

    }

    @Override
    public void sendToLogin() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    public void sendToHome() {
        startActivity(new Intent(this, Home.class));
        finish();
    }

    @Override
    public void sendToLocation() {
        startActivity(new Intent(this, DetectLocation.class));
        finish();
    }
}
