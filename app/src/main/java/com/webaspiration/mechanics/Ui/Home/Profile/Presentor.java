package com.webaspiration.mechanics.Ui.Home.Profile;

/**
 * Created by darkapple on 09/11/17.
 */

public class Presentor implements MVP.Presentor {

    MVP.View view;
    MVP.Model model;

    public Presentor(MVP.Model model) {
        this.model = model;
    }

    @Override
    public void destroy() {
        model.destroy();
    }

    @Override
    public void setView(MVP.View c) {
        this.view=c;
    }

    @Override
    public void init() {

    }
}
