package com.webaspiration.mechanics.Ui.ViewProduct;

import com.webaspiration.mechanics.Dagger.Provides.Client;
import com.webaspiration.mechanics.Dagger.Retention.PerActivity;
import com.webaspiration.mechanics.Network.Network;

import dagger.Provides;

@dagger.Module
public class Module {


    @Provides
    @PerActivity
    MVP.Model model(Client client) {
        return new Controller(client.mClient);
    }

    @Provides
    @PerActivity
    MVP.Presentor presentor(MVP.Model model) {
        return new Presentor(model);
    }

}