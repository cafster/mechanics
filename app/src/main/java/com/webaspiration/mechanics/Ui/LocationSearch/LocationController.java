package com.webaspiration.mechanics.Ui.LocationSearch;


import com.webaspiration.mechanics.Common.Constants;
import com.webaspiration.mechanics.Dagger.Provides.Client;
import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Place.Place;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by shaan on 10/12/2017.
 */

public class LocationController implements MVP.Model {


    private CompositeDisposable disposable;
    private Place place;

    public LocationController() {
        disposable=new CompositeDisposable();
        place= Client.place();
    }

    @Override
    public void search(String input, String filter, NetworkInteractor a) {
        disposable.add(place.predictions(Constants.googleApiKey,input,filter).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(a::handle,a::error));
    }
    @Override
    public void search(String input, NetworkInteractor a) {
        disposable.add(place.predictions(Constants.googleApiKey,input).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(a::handle,a::error));
    }

    @Override
    public void destroy() {
        if(!disposable.isDisposed())
            disposable.dispose();
    }
}
