package com.webaspiration.mechanics.Ui.Home.MyAds.OldProducts;

import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Beans.ProductResponse;

import retrofit2.Response;

/**
 * Created by darkapple on 11/11/17.
 */

public class Presentor implements MVP.Presentor {

    MVP.Model model;
    MVP.View view;

    public Presentor(MVP.Model model) {
        this.model = model;
    }

    @Override
    public void destroy() {
        model.destroy();
    }

    @Override
    public void init() {
        model.load(new NetworkInteractor<ProductResponse>() {
            @Override
            public void handle(Response<ProductResponse> o) {
                if(o.isSuccessful())
                    view.load(o.body());
            }

            @Override
            public void error(Throwable e) {
                view.message(e.getMessage());
            }
        });
    }

    @Override
    public void setView(MVP.View c) {
        this.view=c;
    }
}
