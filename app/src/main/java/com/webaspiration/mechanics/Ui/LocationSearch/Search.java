package com.webaspiration.mechanics.Ui.LocationSearch;

import android.app.Activity;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;


import com.webaspiration.mechanics.Adapter.SearchAdapter;
import com.webaspiration.mechanics.Dagger.Provides.Fonts;
import com.webaspiration.mechanics.R;
import com.webaspiration.mechanics.Ui.Base.BaseActivity;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

public class Search extends BaseActivity implements MVP.View {

    @Inject
    MVP.Presentor presentor;

    @BindView(R.id.search_city)
    EditText search_city;

    @Inject
    Fonts fonts;

    String search = "";

    PublishSubject<String> subject;

    @BindView(R.id.search_list_view)
    ListView search_list_view;


    @Override
    public void initialize() {
        overridePendingTransition(R.anim.slideup, R.anim.nochange);
        presentor.setView(this);
        subject = PublishSubject.create();
        ((TextView)findViewById(R.id.search_result)).setTypeface(fonts.small);
        search_city.setTypeface(fonts.small);
        search_city.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                subject.onNext(search_city.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        subject.skip(1).debounce(1, TimeUnit.SECONDS).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(s -> {
            search=s;
            presentor.search();
        });
    }

    @Override
    public int layout() {
        return R.layout.activity_search;
    }

    @Override
    public void dependency() {
        component().plus(new LocationModule()).inject(this);
    }

    @Override
    public void networkcall() {

    }

    public void goBack(View v) {
        setResult(RESULT_CANCELED);
        onBackPressed();
        overridePendingTransition(R.anim.nochange, R.anim.slidedown);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presentor.destroy();
    }

    @Override
    public String filter() {
        return getIntent().getStringExtra("filters");
    }

    @Override
    public void message(String message) {
        if (toast.snackbar != null && toast.snackbar.isShownOrQueued()) {
            toast.snackbar.dismiss();
        }
        if (coordinatorLayout != null) {
            toast.message(message).on(coordinatorLayout).snackbar().show();
        } else {
            toast.message(message).toast();
        }
    }

    @Override
    public String search() {
        return search;
    }

    @Override
    public void showList(List<String> list) {
        if(list.size()>0)
            ((TextView)findViewById(R.id.search_result)).setVisibility(View.VISIBLE);
        search_list_view.setAdapter(new SearchAdapter(list));
        search_list_view.setOnItemClickListener((parent, view, position, id) -> {
            Intent resultIntent = new Intent();
            resultIntent.putExtra("location", list.get(position));
            setResult(Activity.RESULT_OK, resultIntent);
            overridePendingTransition(R.anim.nochange, R.anim.slidedown);
            finish();
        });
    }


}
