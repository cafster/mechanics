package com.webaspiration.mechanics.Ui.Settings;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.webaspiration.mechanics.Dagger.Provides.Fonts;
import com.webaspiration.mechanics.Dagger.Provides.SessionManager;
import com.webaspiration.mechanics.Dagger.Retention.ActivityContext;
import com.webaspiration.mechanics.Network.Beans.MeResponse.MeResponse;
import com.webaspiration.mechanics.Network.Beans.MeResponse.User;
import com.webaspiration.mechanics.R;
import com.webaspiration.mechanics.Ui.Base.BaseActivity;
import com.webaspiration.mechanics.Ui.ChangePassword.ChangePassword;
import com.webaspiration.mechanics.Ui.Splash.Splash;
import com.webaspiration.mechanics.View.CircleImageView.CircleImageView;

import javax.inject.Inject;

import butterknife.BindView;
import okhttp3.ConnectionPool;

public class Profile extends BaseActivity implements MVP.View{

    @Inject
    SessionManager sessionManager;

    @BindView(R.id.mobile)
    TextView mobile;
    @BindView(R.id.name)
    TextView name;

    @Inject
    MVP.Presentor presentor;

    @Inject
    Fonts fonts;

    @Inject
    @ActivityContext
    Context context;

    @BindView(R.id.changePassword)
    TextView changePassword;

    @BindView(R.id.changePasswordArea)
    RelativeLayout changePasswordArea;

    @Override
    public void load(MeResponse response) {
        User user=response.getUser();
        if(user.getType().equals("Mobile")){
            changePasswordArea.setVisibility(View.VISIBLE);
        }else{
            mobile.setText(response.getUser().getEmail());
        }
        if(user.getImage()!=null && !user.getImage().equals("")){
            Picasso.with(context).load(user.getImage()).into((CircleImageView)findViewById(R.id.profileImage));
        }
    }

    @Override
    public void initialize() {
        overridePendingTransition(R.anim.in_from_right,R.anim.out_to_left);
        name.setTypeface(fonts.bold);
        presentor.setView(this);
        presentor.init();
        changePasswordArea.setVisibility(View.GONE);
        mobile.setTypeface(fonts.small);
        name.setText(sessionManager.name());
        changePassword.setTypeface(fonts.bold);
        ((TextView)findViewById(R.id.changePasswordTitle)).setTypeface(fonts.bold);
        ((TextView)findViewById(R.id.notificationTitle)).setTypeface(fonts.bold);
        ((TextView)findViewById(R.id.notification)).setTypeface(fonts.bold);
        ((TextView)findViewById(R.id.appVersion)).setTypeface(fonts.bold);
        ((TextView)findViewById(R.id.appVersionTitle)).setTypeface(fonts.bold);
        ((TextView)findViewById(R.id.logouttitle)).setTypeface(fonts.bold);
        ((TextView)findViewById(R.id.logout)).setTypeface(fonts.bold);
        mobile.setText(sessionManager.mobile());
        ((Button)findViewById(R.id.inviteFriend)).setTypeface(fonts.small);
        ((Button)findViewById(R.id.inviteFriend)).setOnClickListener(view -> {
            String appPackageName = context.getPackageName();
            String textToSend = "Hey!! Please download this awesome app to find deals near you  .\nClick: " + "https://play.google.com/store/apps/details?id=" + appPackageName;
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, textToSend);
            sendIntent.setType("text/plain");
            startActivity(Intent.createChooser(sendIntent, "Please Choose App"));
        });
        ((TextView)findViewById(R.id.logout)).setOnClickListener(view -> {
            sessionManager.logout();
            startActivity(new Intent(context, Splash.class));
           finish();
        });
        changePassword.setOnClickListener(view -> startActivity(new Intent(this, ChangePassword.class)));
        ((TextView)findViewById(R.id.notification)).setOnClickListener(view ->{
            Intent intent = new Intent();
            if(android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1){
                intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
                intent.putExtra("android.provider.extra.APP_PACKAGE", context.getPackageName());
            }else if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
                intent.putExtra("app_package", context.getPackageName());
                intent.putExtra("app_uid", context.getApplicationInfo().uid);
            }else {
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                intent.addCategory(Intent.CATEGORY_DEFAULT);
                intent.setData(Uri.parse("package:" + context.getPackageName()));
            }
            context.startActivity(intent);
        });
    }

    @Override
    public int layout() {
        return R.layout.activity_profile;
    }

    @Override
    public void dependency() {
        component().plus(new Module()).inject(this);
    }

    @Override
    public void networkcall() {

    }

    @Override
    public void message(String message) {
        if (toast.snackbar != null && toast.snackbar.isShownOrQueued()) {
            toast.snackbar.dismiss();
        }
        if (coordinatorLayout != null) {
            toast.message(message).on(coordinatorLayout).snackbar().show();
        } else {
            toast.message(message).toast();
        }
    }
}
