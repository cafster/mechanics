package com.webaspiration.mechanics.Ui.Home.MyAds;

import com.webaspiration.mechanics.Network.Network;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by darkapple on 09/11/17.
 */

public class Controller implements MVP.Model{

    Network mNetwork;
    CompositeDisposable disposable;

    public Controller(Network mNetwork) {
        this.mNetwork = mNetwork;
        disposable=new CompositeDisposable();
    }

    @Override
    public void destroy() {
        if(!disposable.isDisposed())
            disposable.dispose();
    }
}
