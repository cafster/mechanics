package com.webaspiration.mechanics.Ui.Search;

import com.webaspiration.mechanics.Common.Validates;
import com.webaspiration.mechanics.Dagger.Provides.Client;
import com.webaspiration.mechanics.Dagger.Provides.SessionManager;
import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Beans.ProductResponse;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class Controller implements MVP.Model {

    CompositeDisposable disposable;
    SessionManager sessionManager;

    public Controller(SessionManager sessionManager) {
        this.sessionManager = sessionManager;
        disposable = new CompositeDisposable();
    }


    @Override
    public void destroy() {
        if (!disposable.isDisposed())
            disposable.dispose();
    }

    @Override
    public void search(String search, NetworkInteractor<ProductResponse> handler) {
        disposable.add(Client.mClient.products(search, String.valueOf(sessionManager.getLocation().latitude), String.valueOf(sessionManager.getLocation().longitude))
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).flatMap(new Validates()).subscribe(handler::handle, handler::error));
    }

}