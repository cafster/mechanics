package com.webaspiration.mechanics.Ui.Home.Chat;

import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Beans.ChatResponse.AllChatResponse;

/**
 * Created by darkapple on 09/11/17.
 */

public interface MVP {

    interface View{
        void message(String message);
        void load(AllChatResponse response);
    }

    interface Presentor{
        void destroy();
        void init();
        void setView(View c);
    }

    interface Model{
        void destroy();
        void load(NetworkInteractor<AllChatResponse> handler);
    }

}
