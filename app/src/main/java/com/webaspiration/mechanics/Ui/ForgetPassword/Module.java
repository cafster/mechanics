package com.webaspiration.mechanics.Ui.ForgetPassword;

import com.webaspiration.mechanics.Dagger.Retention.PerActivity;

import dagger.Provides;

@dagger.Module
public class Module {


    @Provides
    @PerActivity
    MVP.Model model() {
        return new Controller();
    }

    @Provides
    @PerActivity
    MVP.Presentor presentor(MVP.Model model) {
        return new Presentor(model);
    }

}