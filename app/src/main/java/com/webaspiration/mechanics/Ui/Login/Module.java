package com.webaspiration.mechanics.Ui.Login;


import com.webaspiration.mechanics.Dagger.Provides.Client;
import com.webaspiration.mechanics.Dagger.Provides.SessionManager;
import com.webaspiration.mechanics.Dagger.Retention.PerActivity;

import dagger.Provides;

/**
 * Created by shaan on 10/3/2017.
 */

@dagger.Module
public class Module {

    @Provides
    @PerActivity
    MVP.Model model(Client client){return new Controller(client);}

    @Provides
    @PerActivity
    MVP.Presentor presentor(MVP.Model model,SessionManager sessionManager){
        return new Presentor(model,sessionManager);
    }

}
