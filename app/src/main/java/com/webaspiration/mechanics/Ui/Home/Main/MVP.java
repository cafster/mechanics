package com.webaspiration.mechanics.Ui.Home.Main;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;
import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Beans.ProductResponse;

/**
 * Created by darkapple on 09/11/17.
 */

public interface MVP {

    interface View{
        void message(String message);
        void load(ProductResponse response);
        void setLocation(String location);
        Context c();
    }

    interface Presentor{
        void destroy();
        void init();
        void setView(View c);
        void saveLocation(LatLng latLng);
    }

    interface Model{
        void destroy();
        void clear();
        void load(NetworkInteractor<ProductResponse>handler);
    }

}
