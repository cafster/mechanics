package com.webaspiration.mechanics.Ui.Home.Chat;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.webaspiration.mechanics.Adapter.AllChatAdapter;
import com.webaspiration.mechanics.Dagger.Provides.Fonts;
import com.webaspiration.mechanics.Dagger.Retention.ActivityContext;
import com.webaspiration.mechanics.Network.Beans.ChatResponse.AllChatResponse;
import com.webaspiration.mechanics.R;
import com.webaspiration.mechanics.Ui.Base.BaseFragment;
import com.webaspiration.mechanics.Ui.Chat.ChatActivity;
import com.webaspiration.mechanics.View.RecyclerItemClickListener;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class Chat extends BaseFragment implements MVP.View {


    com.webaspiration.mechanics.Ui.Home.MVP.View view;

    @Inject
    MVP.Presentor presentor;

    @Override
    public void message(String message) {
        view.message(message);
    }

    @Inject
    Fonts f;

    @Inject
    @ActivityContext
    Context c;

    @BindView(R.id.error)
    TextView error;

    @Override
    public void load(AllChatResponse response) {
        List<com.webaspiration.mechanics.Network.Beans.ChatResponse.Chat> list = response.getChats();
        AllChatAdapter adapter = new AllChatAdapter(c, list, f);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(c));
        if (list.size() == 0) {
            error.setText("No Recent Chats Found");
        }
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(c, (view1, position) -> {
            com.webaspiration.mechanics.Network.Beans.ChatResponse.Chat chat = list.get(position);
            Intent i = new Intent(c, ChatActivity.class);
            i.putExtra("id", chat.getProduct().getId().toString());
            if (chat.getSender() == 0)
                i.putExtra("uid", chat.getFrom().getId().toString());
            else
                i.putExtra("uid", chat.getTo().getId().toString());
            startActivity(i);
        }));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        view = (com.webaspiration.mechanics.Ui.Home.MVP.View) getActivity();
        view.comp().plus(new Module()).inject(this);
        presentor.setView(this);
    }


    @Override
    protected void onconnected() {
        presentor.init();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presentor.destroy();
    }

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    @Override
    protected void oncreateview(View v) {
        ((TextView) v.findViewById(R.id.title)).setTypeface(f.small);
    }

    @Override
    protected int layout() {
        return R.layout.fragment_chat;
    }
}
