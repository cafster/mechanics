package com.webaspiration.mechanics.Ui.Settings;

import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Beans.MeResponse.MeResponse;
import com.webaspiration.mechanics.Network.Network;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class Controller implements MVP.Model {

    Network mClient;
    CompositeDisposable disposable;

    public Controller(Network mClient) {
        this.mClient = mClient;
        disposable = new CompositeDisposable();
    }

    @Override
    public void me(NetworkInteractor<MeResponse> responseNetworkInteractor) {
        mClient.me().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(responseNetworkInteractor::handle,responseNetworkInteractor::error);
    }

    @Override
    public void destroy() {
        if (!disposable.isDisposed())
            disposable.dispose();
    }

}