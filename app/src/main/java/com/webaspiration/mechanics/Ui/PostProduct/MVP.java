package com.webaspiration.mechanics.Ui.PostProduct;

import android.content.Context;

import com.nguyenhoanglam.imagepicker.model.Image;
import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Beans.CategoryResponse;
import com.webaspiration.mechanics.Network.Beans.ProductPostResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;

/**
 * Created by darkapple on 13/11/17.
 */

public interface MVP {

    interface View{
        void message(String message);
        void sendBack();
        void showMessage();
        String name();
        Long category();
        String price();
        Context c();
        String descriptions();
        ArrayList<Image> images();
        void load(CategoryResponse response);
    }

    interface Presentor{
        void setView(View c);
        void destroy();
        void post();
        void init();
    }

    interface Model{
        void destroy();
        String lat();
        String lng();
        void postProduct(HashMap<String,String>map , NetworkInteractor<ProductPostResponse>responseNetworkInteractor);
        void postImage(String id, MultipartBody.Part body);
        void load(NetworkInteractor<CategoryResponse>handler);
    }
}
