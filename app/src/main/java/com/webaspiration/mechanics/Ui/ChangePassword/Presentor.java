package com.webaspiration.mechanics.Ui.ChangePassword;


import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Model.Success;

import retrofit2.Response;

/**
 * Created by darkapple on 27/11/17.
 */

public class Presentor implements MVP.Presentor {

    MVP.Model model;
    MVP.View view;

    public Presentor(MVP.Model m) {
        this.model = m;
    }

    @Override
    public void destroy() {
        model.destroy();
    }

    @Override
    public void init() {
    }

    @Override
    public void changePass() {
        if(view.npass().length()<4)
        {
            view.message("Enter a valid password");
            return;
        }
        if(view.npass().equals(view.cpaass())){
            model.changePass(view.npass(), new NetworkInteractor<Success>() {
                @Override
                public void handle(Response<Success> o) {
                    if(o.isSuccessful()){
                        view.sendBack();
                    }
                }

                @Override
                public void error(Throwable e) {

                }
            });
        }else{
            view.message("Password doesnt match");
        }
    }

    @Override
    public void setView(MVP.View c) {
        this.view = c;
    }

}
