package com.webaspiration.mechanics.Ui.ViewProduct;

import com.webaspiration.mechanics.Common.Validates;
import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Beans.GetProductResponse;
import com.webaspiration.mechanics.Network.Model.Success;
import com.webaspiration.mechanics.Network.Network;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class Controller implements MVP.Model {

    Network mClient;
    CompositeDisposable disposable;

    public Controller(Network mClient) {
        this.mClient = mClient;
        disposable = new CompositeDisposable();
    }

    @Override
    public void delete(String id, NetworkInteractor<Success> responseNetworkInteractor) {
        disposable.add(mClient.deleteProduct(id).flatMap(new Validates()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(responseNetworkInteractor::handle,responseNetworkInteractor::error));

    }

    @Override
    public void load(String id, NetworkInteractor<GetProductResponse> responseNetworkInteractor) {
        disposable.add(mClient.getProduct(id).flatMap(new Validates()).subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread()).subscribe(responseNetworkInteractor::handle,responseNetworkInteractor::error));
    }

    @Override
    public void destroy() {
        if (!disposable.isDisposed())
            disposable.dispose();
    }

}