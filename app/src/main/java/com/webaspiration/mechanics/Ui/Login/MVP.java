package com.webaspiration.mechanics.Ui.Login;

import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Beans.User;

import java.util.HashMap;

/**
 * Created by darkapple on 05/11/17.
 */

public interface MVP {

    interface View{
        void message(String message);
        String name();
        String password();
        void sendtohome();
    }

    interface Presentor{
        void setView(View c);
        void destroy();
        void login(HashMap<String,String> map);
        void login();
    }

    interface Model{
        void login(String name, String password, NetworkInteractor<User> handler);
        void login(HashMap<String,String> map, NetworkInteractor<User> handler);
        void destroy();

    }

}
