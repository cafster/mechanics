package com.webaspiration.mechanics.Ui.Chat;

import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Beans.LoadChatResponse.LoadChatResponse;
import com.webaspiration.mechanics.Network.Beans.UnseenResponse.UnseenResponse;
import com.webaspiration.mechanics.Network.Model.Success;

public interface MVP {


    interface View {
        void message(String message);
        String id();
        String uid();
        void load(LoadChatResponse response);
        void load(UnseenResponse response);
    }

    interface Model {
        void destroy();
        void load(String a,String b,NetworkInteractor<LoadChatResponse>handler);
        void unseen(String a,String b,NetworkInteractor<UnseenResponse>handler);
        void  post(String a,String b,String body,NetworkInteractor<Success>handler);
    }

    interface Presentor {
        void destroy();
        void init();
        void reload();
        void postMessage(String a);
        void setView(View c);
    }


}