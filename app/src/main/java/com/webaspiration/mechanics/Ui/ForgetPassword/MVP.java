package com.webaspiration.mechanics.Ui.ForgetPassword;

import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Model.Error;

public interface MVP {


    interface View {
        void message(String message);
        String number();
        void sendBack();
    }

    interface Model {
        void destroy();
        void resetCode(String number,NetworkInteractor<Error> handler);
    }

    interface Presentor {
        void destroy();
        void init();
        void reset();
        void setView(View c);
    }


}