package com.webaspiration.mechanics.Ui.Search;

import com.webaspiration.mechanics.Dagger.Provides.SessionManager;
import com.webaspiration.mechanics.Dagger.Retention.PerActivity;

import dagger.Provides;

@dagger.Module
public class Module {


    @Provides
    @PerActivity
    MVP.Model model(SessionManager sessionManager) {
        return new Controller(sessionManager);
    }

    @Provides
    @PerActivity
    MVP.Presentor presentor(MVP.Model model) {
        return new Presentor(model);
    }

}