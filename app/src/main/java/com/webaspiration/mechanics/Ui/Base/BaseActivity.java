package com.webaspiration.mechanics.Ui.Base;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;


import com.webaspiration.mechanics.Dagger.Component.ActivityComponent;
import com.webaspiration.mechanics.Dagger.Component.DaggerActivityComponent;
import com.webaspiration.mechanics.Dagger.Module.ActivityModule;
import com.webaspiration.mechanics.Dagger.Provides.MyToast;
import com.webaspiration.mechanics.Events.NetworkStateChanged;
import com.webaspiration.mechanics.R;
import com.webaspiration.mechanics.Service.NetworkStateReceiver;
import com.webaspiration.mechanics.View.MyApplication;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {



    NetworkStateReceiver mReciever;
    private ActivityComponent component;

    @BindView(R.id.coordinator)
    public CoordinatorLayout coordinatorLayout;

    @Inject
    protected MyToast toast;


    boolean networkcall=false;

    public void facebook(){

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        facebook();
        setContentView(layout());
        mReciever = new NetworkStateReceiver();
        ButterKnife.bind(this);
        component= DaggerActivityComponent.builder().activityModule(new ActivityModule(this)).applicationComponent(MyApplication.component).build();
        dependency();
        initialize();
    }


    public ActivityComponent component(){
        if(component==null){
            component= DaggerActivityComponent.builder().activityModule(new ActivityModule(this)).applicationComponent(MyApplication.component).build();
        }
        return component;
    }


    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        this.registerReceiver(mReciever, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(toast.snackbar!=null&&toast.snackbar.isShownOrQueued()){
            toast.snackbar.dismiss();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        this.unregisterReceiver(mReciever);
    }

    public abstract void initialize();
    public abstract int layout();
    public abstract void dependency();
    public abstract void networkcall();

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNetworkStateChangedEvent(NetworkStateChanged event) {
        if (!event.isInternetConnected()) {
            ondisconnected();
        } else {
            onconnected();
        }
    }

    public void ondisconnected() {
        if (coordinatorLayout != null) {
            toast.message("No Internet Connection Available.").on(coordinatorLayout).snackbar().show("Settings", () -> {
                Intent intent = new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS);
                startActivity(intent);
            });
        } else {
            toast.message("No Internet Connection").toast();
        }
    }

    public void onconnected() {
        if(toast.snackbar!=null&&toast.snackbar.isShownOrQueued()){
            toast.snackbar.dismiss();
        }
        if(!networkcall){
            networkcall=true;
            networkcall();
        }
    }


}
