package com.webaspiration.mechanics.Ui.Signup;

import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Model.Success;

import java.util.HashMap;

/**
 * Created by darkapple on 05/11/17.
 */

public interface MVP {

    interface View{
        void message(String message);
        String name();
        String email();
        String mobile();
        String password();
        void sendtologin();
        void hideOtp();
        void showOtp();
        void sendOtp();
    }

    interface Presentor{
        void setView(View c);
        void destroy();
        void signUp();
        void sendOtp();
    }

    interface Model{
        void signUp(HashMap<String,String>map, NetworkInteractor<Success> handler);
        void destroy();

    }

}
