package com.webaspiration.mechanics.Ui.Chat;


import android.graphics.Color;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.widget.EditText;
import android.widget.TextView;

import com.webaspiration.mechanics.Dagger.Provides.Fonts;
import com.webaspiration.mechanics.Dagger.Provides.SessionManager;
import com.webaspiration.mechanics.Network.Beans.LoadChatResponse.Chat;
import com.webaspiration.mechanics.Network.Beans.LoadChatResponse.Data;
import com.webaspiration.mechanics.Network.Beans.LoadChatResponse.LoadChatResponse;
import com.webaspiration.mechanics.Network.Beans.LoadChatResponse.Product;
import com.webaspiration.mechanics.Network.Beans.UnseenResponse.UnseenResponse;
import com.webaspiration.mechanics.R;
import com.webaspiration.mechanics.Ui.Base.BaseActivity;
import com.webaspiration.mechanics.View.message.Message;
import com.webaspiration.mechanics.View.message.MessageView;
import com.webaspiration.mechanics.View.message.User;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Observable;
import java.util.stream.Collectors;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class ChatActivity extends BaseActivity implements MVP.View{

    @Inject
    Fonts fonts;

    @Inject
    MVP.Presentor presentor;

    @BindView(R.id.chat_text)
    EditText chat_text;

    @BindView(R.id.message_view)
    MessageView mChatView;


    User meuser;
    User uuser;


    @Override
    public void initialize() {
        presentor.setView(this);
        overridePendingTransition(R.anim.in_from_right,R.anim.out_to_left);
        title.setTypeface(fonts.bold);
        chat_text.setTypeface(fonts.small);
        runnable=new Runnable() {
            @Override
            public void run() {
                presentor.reload();
                ha.postDelayed(runnable,3000);
            }
        };
        mChatView.setRightBubbleColor(Color.parseColor("#6096BA"));
        mChatView.setLeftBubbleColor(Color.parseColor("#3C4859"));
        mChatView.setBackgroundColor(Color.parseColor("#ffffff"));
        mChatView.setRightMessageTextColor(Color.WHITE);
        mChatView.setLeftMessageTextColor(Color.WHITE);
        mChatView.setUsernameTextColor(Color.parseColor("#000000"));
        mChatView.setSendTimeTextColor(ContextCompat.getColor(this,R.color.header));
        mChatView.setMessageMarginTop(15);
        mChatView.setMessageMarginBottom(15);
        mChatView.setOnKeyboardAppearListener(new MessageView.OnKeyboardAppearListener() {
            @Override
            public void onKeyboardAppeared(boolean hasChanged) {
                //Appeared keyboard
                if (hasChanged) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Scroll to end
                            mChatView.scrollToEnd();
                        }
                    }, 200);
                }
            }
        });
    }

    @OnClick(R.id.goBack)
    public void goBack(){
        onBackPressed();
        finish();
    }

    @BindView(R.id.title)
    TextView title;

    @Override
    public int layout() {
        return R.layout.activity_chat;
    }

    @Override
    public void dependency() {
        component().plus(new Module()).inject(this);
    }

    @Override
    public void networkcall() {
        presentor.init();
    }

    @Inject
    SessionManager sessionManager;

    @Override
    public void load(LoadChatResponse response) {
        Data data=response.getData();
        Product product=data.getProduct();
        title.setText(product.getName());
        com.webaspiration.mechanics.Network.Beans.LoadChatResponse.User user=data.getUser();
        uuser = new User(0, user.getName(),user.getImage());
        meuser = new User(1, sessionManager.name(),sessionManager.image());
        List<Chat> chats=data.getChats();
        for (Chat msg:chats) {
            if(msg.getUser()==1){
                Message mesg=new Message.Builder().setUser(meuser).setRightMessage(true)
                        .setCreatedAt(calender(msg.getCreatedAt())).setMessageText(msg.getBody()).build();
                mChatView.setMessage(mesg);
            }else{
                Message mesg=new Message.Builder().setUser(uuser).setRightMessage(false)
                        .setCreatedAt(calender(msg.getCreatedAt())).setMessageText(msg.getBody()).build();
                mChatView.setMessage(mesg);
            }
        }
        mChatView.scrollToEnd();
    }

    @Override
    public String id() {
        return getIntent().getStringExtra("id");
    }

    @Override
    public String uid() {
        return getIntent().getStringExtra("uid");
    }

    @Override
    public void message(String message) {
        if (toast.snackbar != null && toast.snackbar.isShownOrQueued()) {
            toast.snackbar.dismiss();
        }
        if (coordinatorLayout != null) {
            toast.message(message).on(coordinatorLayout).snackbar().show();
        } else {
            toast.message(message).toast();
        }
    }

    @OnClick(R.id.sendMessage)
    public void sendMessage(){
        String text=chat_text.getText().toString();
        chat_text.setText("");
        presentor.postMessage(text);
        Message mesg=new Message.Builder().setUser(meuser).setRightMessage(true)
                .setCreatedAt(Calendar.getInstance()).setMessageText(text).build();
        mChatView.setMessage(mesg);
        mChatView.scrollToEnd();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startReload();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presentor.destroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopReload();
    }

    void stopReload(){
        ha.removeCallbacks(runnable);
    }

    final Handler ha=new Handler();
    Runnable runnable;
    void startReload(){
        ha.postDelayed(runnable, 3000);
    }

    @Override
    public void load(UnseenResponse response) {
        List<com.webaspiration.mechanics.Network.Beans.UnseenResponse.Chat> chats=response.getChats();
        for(com.webaspiration.mechanics.Network.Beans.UnseenResponse.Chat chat:chats){
            Message mesg=new Message.Builder().setUser(uuser).setRightMessage(false)
                    .setCreatedAt(calender(chat.getCreatedAt())).setMessageText(chat.getBody()).build();
            mChatView.setMessage(mesg);
        }
        mChatView.scrollToEnd();
    }

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
    Calendar calender(String daate){
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        try {
            date = (Date)dateFormat.parse(daate);
            cal = Calendar.getInstance();
            cal.setTime(date);
            return cal;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return cal;
    }
}
