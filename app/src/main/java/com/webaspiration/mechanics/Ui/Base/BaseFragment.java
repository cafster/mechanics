package com.webaspiration.mechanics.Ui.Base;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.webaspiration.mechanics.Events.NetworkStateChanged;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.ButterKnife;

/**
 * Created by shaan on 9/3/2017.
 */

@SuppressWarnings("DefaultFileTemplate")
public abstract class BaseFragment extends Fragment {


    protected boolean networkcall=false;
    protected abstract void onconnected();
    protected abstract void oncreateview(View v);
    protected abstract int layout();

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNetworkStateChangedEvent(NetworkStateChanged event) {
        if (event.isInternetConnected() && !networkcall) {
            networkcall=true;
            onconnected();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        if(isNetworkAvailable(getActivity().getBaseContext()) && !networkcall){
            networkcall=true;
            onconnected();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
        View v=inflater.inflate(layout(), container, false);
        ButterKnife.bind(this,v);
        oncreateview(v);
        return v;
    }

    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

}
