package com.webaspiration.mechanics.Ui.ChangePassword;

import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Model.Success;

public interface MVP {


    interface View {
        void message(String message);
        String npass();
        String cpaass();
        void sendBack();
    }

    interface Model {
        void destroy();
        void changePass(String password, NetworkInteractor<Success>handler);
    }

    interface Presentor {
        void destroy();
        void init();
        void changePass();
        void setView(View c);
    }


}