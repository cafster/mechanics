package com.webaspiration.mechanics.Ui.ChangePassword;

import com.webaspiration.mechanics.Common.Validates;
import com.webaspiration.mechanics.Dagger.Provides.Client;
import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Model.Success;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class Controller implements MVP.Model {

    private CompositeDisposable disposable;

    public Controller() {
        disposable = new CompositeDisposable();
    }

    @Override
    public void destroy() {
        if (!disposable.isDisposed())
            disposable.dispose();
    }

    @Override
    public void changePass(String password, NetworkInteractor<Success> handler) {
        disposable.add(Client.mClient.password(password).flatMap(new Validates())
        .observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
        .subscribe(handler::handle,handler::error));
    }
}