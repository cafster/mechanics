package com.webaspiration.mechanics.Ui.Settings;


import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Beans.MeResponse.MeResponse;

import retrofit2.Response;

/**
 * Created by darkapple on 14/11/17.
 */

public class Presentor implements MVP.Presentor {

    MVP.Model model;
    MVP.View view;

    public Presentor(MVP.Model m) {
        this.model = m;
    }

    @Override
    public void destroy() {
        model.destroy();
    }

    @Override
    public void init() {
        model.me(new NetworkInteractor<MeResponse>() {
            @Override
            public void handle(Response<MeResponse> o) {
                if(o.isSuccessful())
                    view.load(o.body());
            }

            @Override
            public void error(Throwable e) {

            }
        });
    }

    @Override
    public void setView(MVP.View c) {
        this.view = c;
    }

}
