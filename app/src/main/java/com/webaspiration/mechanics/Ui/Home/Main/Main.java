package com.webaspiration.mechanics.Ui.Home.Main;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.gms.maps.model.LatLng;
import com.webaspiration.mechanics.Adapter.HomeProductAdapter;
import com.webaspiration.mechanics.Adapter.SearchAdapter;
import com.webaspiration.mechanics.Common.Common;
import com.webaspiration.mechanics.Dagger.Provides.Fonts;
import com.webaspiration.mechanics.Dagger.Retention.ActivityContext;
import com.webaspiration.mechanics.Events.LocationEvent;
import com.webaspiration.mechanics.Network.Beans.Product;
import com.webaspiration.mechanics.Network.Beans.ProductResponse;
import com.webaspiration.mechanics.R;
import com.webaspiration.mechanics.Ui.Base.BaseFragment;
import com.webaspiration.mechanics.Ui.LocationSearch.Search;
import com.webaspiration.mechanics.Ui.ViewProduct.ViewProduct;
import com.webaspiration.mechanics.View.RecyclerItemClickListener;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;


public class Main extends BaseFragment implements MVP.View {

    com.webaspiration.mechanics.Ui.Home.MVP.View view;

    @Inject
    MVP.Presentor presentor;

    @Inject
    Fonts fonts;

    @ActivityContext
    @Inject
    Context context;

    @BindView(R.id.location)
    TextView locationText;

    boolean isLocationDone=false;

    @Override
    public void setLocation(String location) {
        locationText.setText(location);
    }

    @Override
    public Context c() {
        return context;
    }

    int LocationRequest = 201;
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LocationRequest && resultCode == Activity.RESULT_OK) {
            String location = data.getStringExtra("location");
            LatLng destination= Common.getLocationFromAddress(context,location);
            presentor.saveLocation(destination);
            presentor.init();
        }
    }

    @Override
    public void message(String message) {
        view.message(message);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        view=(com.webaspiration.mechanics.Ui.Home.MVP.View)getActivity();
        view.comp().plus(new Module()).inject(this);
        presentor.setView(this);
    }

    @Override
    protected void onconnected() {
        presentor.init();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presentor.destroy();
    }

    int _dy=0;
    @Override
    protected void oncreateview(View v) {
        ((TextView)v.findViewById(R.id.title_des)).setTypeface(fonts.small);
        locationText.setTypeface(fonts.bold);
        (v.findViewById(R.id.locationImage)).setOnClickListener(view1 -> startActivityForResult(new Intent(context, Search.class),LocationRequest));
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0 ||dy<0 && view.shown())
                {
                    view.hideFabButton();
                }
                super.onScrolled(recyclerView, dx, dy);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE)
                {
                    view.showFabButton();
                }
            }
        });

    }

    @Override
    protected int layout() {
        return R.layout.fragment_main;
    }

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.animation)
    LinearLayout animation;

    @OnClick(R.id.search)
    public void search(){
        startActivity(new Intent(context, com.webaspiration.mechanics.Ui.Search.Search.class));
    }

    @Override
    public void load(ProductResponse response) {
        if(response.getProducts().size()==0){
            animation.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }else{
            animation.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            HomeProductAdapter adapter=new HomeProductAdapter(context,response.getProducts(),fonts);
            recyclerView.setHasFixedSize(true);
            StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, 1);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(adapter);
            recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(context,(view1, position) -> {
                Product product=response.getProducts().get(position);
                Intent i = new Intent(context, ViewProduct.class);
                i.putExtra("product", com.webaspiration.mechanics.Dagger.Provides.Common.getGson().toJson(product));
                startActivity(i);
            }));
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onLocationChangeEvent(LocationEvent event) {
       if(isLocationDone){
           view.endLocation();
       }else{
           isLocationDone=true;
           view.endLocation();
           presentor.saveLocation(new LatLng(event.getLocation().getLatitude(),event.getLocation().getLongitude()));
           presentor.init();
       }
    }


}
