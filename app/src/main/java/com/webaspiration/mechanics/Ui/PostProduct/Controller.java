package com.webaspiration.mechanics.Ui.PostProduct;

import com.webaspiration.mechanics.Common.Validates;
import com.webaspiration.mechanics.Dagger.Provides.Client;
import com.webaspiration.mechanics.Dagger.Provides.SessionManager;
import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Beans.CategoryResponse;
import com.webaspiration.mechanics.Network.Beans.ProductPostResponse;
import com.webaspiration.mechanics.Network.Beans.User;
import com.webaspiration.mechanics.Network.Model.Success;
import com.webaspiration.mechanics.Network.Network;

import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by darkapple on 05/11/17.
 */

public class Controller implements MVP.Model {

    Network api;
    CompositeDisposable disposable;
    SessionManager sessionManager;
    public Controller(Client api, SessionManager sessionManager) {
        this.api = api.mClient;
        this.sessionManager=sessionManager;
        disposable=new CompositeDisposable();
    }

    @Override
    public void load(NetworkInteractor<CategoryResponse> handler) {
        disposable.add(api.getCatges().flatMap(new Validates()).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(handler::handle,handler::error));
    }

    @Override
    public String lat() {
        return String.valueOf(sessionManager.getLocation().latitude);
    }

    @Override
    public String lng() {
        return String.valueOf(sessionManager.getLocation().longitude);
    }

    @Override
    public void postImage(String id, MultipartBody.Part body) {
        api.postImage(id,body).enqueue(new Callback<Success>() {
            @Override
            public void onResponse(Call<Success> call, Response<Success> response) {

            }

            @Override
            public void onFailure(Call<Success> call, Throwable t) {

            }
        });
    }

    @Override
    public void postProduct(HashMap<String, String> map, NetworkInteractor<ProductPostResponse> responseNetworkInteractor) {
        api.postProfuct(map).flatMap(new Validates()).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(responseNetworkInteractor::handle,responseNetworkInteractor::error);
    }

    @Override
    public void destroy() {
        if(!disposable.isDisposed())
            disposable.dispose();
    }
}
