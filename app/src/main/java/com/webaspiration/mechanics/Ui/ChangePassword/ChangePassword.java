package com.webaspiration.mechanics.Ui.ChangePassword;

import android.app.Activity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.webaspiration.mechanics.Dagger.Provides.Fonts;
import com.webaspiration.mechanics.R;
import com.webaspiration.mechanics.Ui.Base.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;

public class ChangePassword extends BaseActivity implements MVP.View{


    @Inject
    MVP.Presentor presentor;

    @Inject
    Fonts fonts;

    @BindView(R.id.changePassword)
    TextView changePassword;

    @BindView(R.id.newPassword)
    EditText newPassword;

    @BindView(R.id.confirmPassword)
    EditText confirmPassword;


    @Override
    public void initialize() {
        presentor.setView(this);
        overridePendingTransition(R.anim.in_from_right,R.anim.out_to_left);
        changePassword.setTypeface(fonts.small);
        newPassword.setTypeface(fonts.small);
        confirmPassword.setTypeface(fonts.small);
        changePassword.setOnClickListener(view -> {
            presentor.changePass();
        });
    }

    @Override
    public int layout() {
        return R.layout.activity_change_password;
    }

    @Override
    public void dependency() {
        component().plus(new Module()).inject(this);
    }

    @Override
    public void networkcall() {

    }

    @Override
    public String npass() {
        return newPassword.getText().toString();
    }

    @Override
    public String cpaass() {
        return confirmPassword.getText().toString();
    }

    @Override
    public void sendBack() {
        toast.message("Password Changed Successfully!").on(coordinatorLayout).snackbar().show("Ok", () -> onBackPressed());
    }

    @Override
    public void message(String message) {
        if (toast.snackbar != null && toast.snackbar.isShownOrQueued()) {
            toast.snackbar.dismiss();
        }
        if (coordinatorLayout != null) {
            toast.message(message).on(coordinatorLayout).snackbar().show();
        } else {
            toast.message(message).toast();
        }
    }

}
