package com.webaspiration.mechanics.Ui.ViewProduct;

import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Beans.GetProductResponse;
import com.webaspiration.mechanics.Network.Model.Success;

public interface MVP {


    interface View {
        void message(String message);
        String id();
        void load(GetProductResponse response);
    }

    interface Model {
        void destroy();
        void delete(String id, NetworkInteractor<Success>responseNetworkInteractor);
        void load(String id, NetworkInteractor<GetProductResponse>responseNetworkInteractor);
    }

    interface Presentor {
        void destroy();
        void delete();
        void init();
        void setView(View c);
    }


}