package com.webaspiration.mechanics.Ui.Home.MyAds.NewProducts;

import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Beans.ProductResponse;

/**
 * Created by darkapple on 11/11/17.
 */

public interface MVP {

    interface View{
        void message(String message);
        void load(ProductResponse response);
    }

    interface Presentor{
        void destroy();
        void init();
        void setView(View c);
    }

    interface Model{
        void destroy();
        void load(NetworkInteractor<ProductResponse>handler);
    }

}
