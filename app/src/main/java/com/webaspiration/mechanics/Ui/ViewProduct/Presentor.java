package com.webaspiration.mechanics.Ui.ViewProduct;


import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Beans.GetProductResponse;
import com.webaspiration.mechanics.Network.Model.Success;

import retrofit2.Response;

/**
 * Created by darkapple on 14/11/17.
 */

public class Presentor implements MVP.Presentor {

    MVP.Model model;
    MVP.View view;

    public Presentor(MVP.Model m) {
        this.model = m;
    }

    @Override
    public void delete() {
        model.delete(view.id(), new NetworkInteractor<Success>() {
            @Override
            public void handle(Response<Success> o) {
                if(o.isSuccessful())
                    view.message("Product Deleted Successfully");
            }

            @Override
            public void error(Throwable e) {
                view.message(e.getMessage());
            }
        });
    }

    @Override
    public void destroy() {
        model.destroy();
    }

    @Override
    public void init() {
        model.load(view.id(), new NetworkInteractor<GetProductResponse>() {
            @Override
            public void handle(Response<GetProductResponse> o) {
                if(o.isSuccessful())
                    view.load(o.body());
            }

            @Override
            public void error(Throwable e) {
                view.message(e.getMessage());
            }
        });
    }

    @Override
    public void setView(MVP.View c) {
        this.view = c;
    }

}
