package com.webaspiration.mechanics.Ui.Home;

import com.webaspiration.mechanics.Dagger.Component.ActivityComponent;

/**
 * Created by darkapple on 09/11/17.
 */

public interface MVP {

    interface View{
        void message(String mess);
        ActivityComponent comp();
        void startLocation();
        void endLocation();
        void hideFabButton();
        void showFabButton();
        boolean shown();
        void finis();
    }

}
