package com.webaspiration.mechanics.Ui.Login;


import com.webaspiration.mechanics.Dagger.Retention.PerActivity;
import com.webaspiration.mechanics.Ui.Splash.Splash;

import dagger.Subcomponent;

@SuppressWarnings("DefaultFileTemplate")
@PerActivity
@Subcomponent(modules = Module.class)
public interface Component {
    void inject(LoginActivity activity);
}