package com.webaspiration.mechanics.Ui.ForgetPassword;


import com.webaspiration.mechanics.Common.SendPlivoMessage;
import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Model.Error;

import retrofit2.Response;

/**
 * Created by darkapple on 27/11/17.
 */

public class Presentor implements MVP.Presentor {

    MVP.Model model;
    MVP.View view;

    public Presentor(MVP.Model m) {
        this.model = m;
    }

    @Override
    public void destroy() {
        model.destroy();
    }

    @Override
    public void init() {
    }

    @Override
    public void reset() {
        if(view.number().length()<10)
        {
            view.message("Please enter a valid number");
            return;
        }
        model.resetCode(view.number(),new NetworkInteractor<Error>() {
            @Override
            public void handle(Response<Error> o) {
                if(o.isSuccessful()){
                    SendPlivoMessage.sendPassword(view.number(),o.body().getError());
                    view.sendBack();
                }
            }

            @Override
            public void error(Throwable e) {
                view.message(e.getMessage());
            }
        });
    }

    @Override
    public void setView(MVP.View c) {
        this.view = c;
    }

}
