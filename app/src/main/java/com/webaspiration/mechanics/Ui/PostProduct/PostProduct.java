package com.webaspiration.mechanics.Ui.PostProduct;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.nguyenhoanglam.imagepicker.model.Config;
import com.nguyenhoanglam.imagepicker.model.Image;
import com.nguyenhoanglam.imagepicker.ui.imagepicker.ImagePicker;
import com.webaspiration.mechanics.Dagger.Provides.Fonts;
import com.webaspiration.mechanics.Dagger.Retention.ActivityContext;
import com.webaspiration.mechanics.Network.Beans.Category;
import com.webaspiration.mechanics.Network.Beans.CategoryResponse;
import com.webaspiration.mechanics.R;
import com.webaspiration.mechanics.Ui.Base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class PostProduct extends BaseActivity implements MVP.View, View.OnClickListener {

    @Inject
    MVP.Presentor presentor;

    @Inject
    Fonts fonts;

    @BindView(R.id.product_name)
    EditText product_name;
    @BindView(R.id.product_price)
    EditText product_price;
    @BindView(R.id.product_description)
    EditText product_description;
    @BindView(R.id.select_images)
    TextView select_images;
    @ActivityContext
    @Inject
    Context c;
    @BindView(R.id.post_product)
    Button post_product;

    @BindView(R.id.spinner)
    MaterialSpinner spinner;

    List<Category> categories;

    @Override
    public void load(CategoryResponse response) {
        categories = response.getCategory();
        List<String> stockList = new ArrayList<String>();
        for (Category category : categories)
            stockList.add(category.getName());
        String[] stockArr = new String[stockList.size()];
        stockArr = stockList.toArray(stockArr);
        spinner.setItems(stockArr);
    }

    @Override
    public Long category() {
        if (categories.size() > 0)
            return categories.get(spinner.getSelectedIndex()).getId();
        else return 0L;
    }

    @Override
    public Context c() {
        return c;
    }

    @Override
    public void initialize() {
        ((TextView) findViewById(R.id.title)).setTypeface(fonts.bold);
        presentor.setView(this);
        product_description.setTypeface(fonts.small);
        product_price.setTypeface(fonts.small);
        product_name.setTypeface(fonts.small);
        post_product.setTypeface(fonts.small);
        post_product.setOnClickListener(this);
        select_images.setTypeface(fonts.small);
        presentor.init();
        select_images.setOnClickListener(view -> {
            PostProductPermissionsDispatcher.pickActivityWithCheck(this);
        });
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.post_product) {
            presentor.post();
            post_product.setOnClickListener(null);
            post_product.setBackgroundColor(Color.parseColor("#666666"));
        }
    }

    @Override
    public int layout() {
        return R.layout.activity_post_product;
    }

    @Override
    public void dependency() {
        component().plus(new Module()).inject(this);
    }

    @Override
    public void networkcall() {

    }

    ArrayList<Image> images = new ArrayList<>();
    int REQUEST_CODE_PICKER = 202;

    @NeedsPermission({Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    void pickActivity() {
        ImagePicker.with(this)
                .setToolbarColor("#08415C")
                .setStatusBarColor("#08415C")
                .setToolbarTextColor("#FFFFFF")
                .setToolbarIconColor("#FFFFFF")
                .setProgressBarColor("#4CAF50")
                .setBackgroundColor("#212121")
                .setCameraOnly(false)
                .setMultipleMode(true)
                .setFolderMode(true)
                .setShowCamera(true)
                .setFolderTitle("Albums")
                .setImageTitle("Galleries")
                .setDoneTitle("Done")
                .setLimitMessage("You have reached selection limit")
                .setMaxSize(4)
                .setSavePath("ImagePicker")
                .setSelectedImages(images)
                .setKeepScreenOn(true)
                .start();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PostProductPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Config.RC_PICK_IMAGES && resultCode == RESULT_OK && data != null) {
            images = data.getParcelableArrayListExtra(Config.EXTRA_IMAGES);
            select_images.setText(String.format("%s images selected", images.size()));
        }
    }

    @Override
    public void message(String message) {
        post_product.setOnClickListener(this);
        post_product.setBackgroundColor(Color.parseColor("#000000"));
        if (toast.snackbar != null && toast.snackbar.isShownOrQueued()) {
            toast.snackbar.dismiss();
        }
        if (coordinatorLayout != null) {
            toast.message(message).on(coordinatorLayout).snackbar().show();
        } else {
            toast.message(message).toast();
        }
    }

    @Override
    public void sendBack() {
        onBackPressed();
        finish();
    }

    @Override
    public void showMessage() {
        toast.message("Product Posted Successfully!").on(coordinatorLayout).snackbar().show("Ok", () -> sendBack());
    }

    @Override
    public String name() {
        return product_name.getText().toString();
    }

    @Override
    public String price() {
        return product_price.getText().toString();
    }

    @Override
    public String descriptions() {
        return product_description.getText().toString();
    }

    @Override
    public ArrayList<Image> images() {
        return images;
    }
}
