package com.webaspiration.mechanics.Ui.LocationSearch;


import com.webaspiration.mechanics.Dagger.Retention.PerActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by shaan on 10/3/2017.
 */

@Module
public class LocationModule {


    @Provides
    @PerActivity
    MVP.Model model(){
        return new LocationController();
    }

    @Provides
    @PerActivity
    MVP.Presentor presentor(MVP.Model model){
        return new LocationPresentor(model);
    }

}
