package com.webaspiration.mechanics.Ui.Home.Chat;

import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Beans.ChatResponse.AllChatResponse;

import retrofit2.Response;

/**
 * Created by darkapple on 09/11/17.
 */

public class Presentor implements MVP.Presentor {

    MVP.View view;
    MVP.Model model;

    public Presentor(MVP.Model model) {
        this.model = model;
    }

    @Override
    public void destroy() {
        model.destroy();
    }

    @Override
    public void setView(MVP.View c) {
        this.view=c;
    }

    @Override
    public void init() {
        model.load(new NetworkInteractor<AllChatResponse>() {
            @Override
            public void handle(Response<AllChatResponse> o) {
                if(o.isSuccessful())
                    view.load(o.body());
            }

            @Override
            public void error(Throwable e) {
                view.message(e.getMessage());
            }
        });
    }
}
