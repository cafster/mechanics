package com.webaspiration.mechanics.Ui.LocationSearch;



import com.webaspiration.mechanics.Dagger.Retention.PerActivity;

import dagger.Subcomponent;

@SuppressWarnings("DefaultFileTemplate")
@PerActivity
@Subcomponent(modules = LocationModule.class)
public interface LocationComponent {
    void inject(Search login);
}