package com.webaspiration.mechanics.Ui.Home.Main;

import com.google.android.gms.maps.model.LatLng;
import com.webaspiration.mechanics.Common.Common;
import com.webaspiration.mechanics.Dagger.Provides.SessionManager;
import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Beans.ProductResponse;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * Created by darkapple on 09/11/17.
 */

public class Presentor implements MVP.Presentor {

    MVP.View view;
    MVP.Model model;
    SessionManager sessionManager;

    public Presentor(MVP.Model model, SessionManager sessionManager) {
        this.model = model;
        this.sessionManager = sessionManager;
    }

    @Override
    public void destroy() {
        model.destroy();
    }

    @Override
    public void setView(MVP.View c) {
        this.view=c;
    }

    @Override
    public void saveLocation(LatLng latLng) {
        sessionManager.saveLocation(latLng);
    }

    @Override
    public void init() {
        model.clear();
        model.load(new NetworkInteractor<ProductResponse>() {
            @Override
            public void handle(Response<ProductResponse> o) {
                if(o.isSuccessful()){
                    view.load(o.body());
                }
            }

            @Override
            public void error(Throwable e) {
                view.message(e.getMessage());
            }
        });
        Observable.just("").map(s -> Common.getCompleteAddressString(view.c(),sessionManager.getLocation())).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(view::setLocation,throwable -> view.message(throwable.getMessage()));
    }
}
