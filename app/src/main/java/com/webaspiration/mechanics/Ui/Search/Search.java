package com.webaspiration.mechanics.Ui.Search;


import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.webaspiration.mechanics.Adapter.SearchProductAdapter;
import com.webaspiration.mechanics.Dagger.Provides.Fonts;
import com.webaspiration.mechanics.Network.Beans.Product;
import com.webaspiration.mechanics.Network.Beans.ProductResponse;
import com.webaspiration.mechanics.R;
import com.webaspiration.mechanics.Ui.Base.BaseActivity;
import com.webaspiration.mechanics.Ui.ViewProduct.ViewProduct;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

public class Search extends BaseActivity implements MVP.View{


    @Inject
    MVP.Presentor presentor;

    @Inject
    Fonts fonts;

    @BindView(R.id.search_city)
    EditText search_city;

    String search = "";

    PublishSubject<String> subject;

    @BindView(R.id.search_list_view)
    ListView search_list_view;

    @Override
    public void initialize() {
        overridePendingTransition(R.anim.in_from_right,R.anim.out_to_left);
        search_city.setTypeface(fonts.small);
        subject = PublishSubject.create();
        search_city.setHint("Enter Search Term");
        search_list_view.setBackground(null);
        adapter=new SearchProductAdapter(fonts,new ArrayList<>());
        search_list_view.setAdapter(adapter);
        search_city.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                subject.onNext(search_city.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        presentor.setView(this);
    }

    @Override
    public int layout() {
        return R.layout.activity_search2;
    }

    @Override
    public void dependency() {
        component().plus(new Module()).inject(this);
    }

    @Override
    public void networkcall() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        subject.skip(1).debounce(1, TimeUnit.SECONDS).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(s -> {
            search=s;
            presentor.search();
        });
    }

    SearchProductAdapter adapter;

    @Override
    public void load(ProductResponse response) {
        List<Product> products=response.getProducts();
        if(products.size()>0)
            ((TextView)findViewById(R.id.search_result)).setVisibility(View.VISIBLE);
        adapter.setProducts(products);
        search_list_view.setOnItemClickListener((adapterView, view, position, l) -> {
            Product product=response.getProducts().get(position);
            Intent i = new Intent(this, ViewProduct.class);
            i.putExtra("product", com.webaspiration.mechanics.Dagger.Provides.Common.getGson().toJson(product));
            startActivity(i);
        });
    }

    @Override
    public String search() {
        return search;
    }

    @OnClick(R.id.goback)
    public void goback(){
        onBackPressed();
    }

    @Override
    public void message(String message) {
        if (toast.snackbar != null && toast.snackbar.isShownOrQueued()) {
            toast.snackbar.dismiss();
        }
        if (coordinatorLayout != null) {
            toast.message(message).on(coordinatorLayout).snackbar().show();
        } else {
            toast.message(message).toast();
        }
    }
}
