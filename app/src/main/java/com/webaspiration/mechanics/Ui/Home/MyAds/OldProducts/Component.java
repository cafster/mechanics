package com.webaspiration.mechanics.Ui.Home.MyAds.OldProducts;


import com.webaspiration.mechanics.Dagger.Retention.PerActivity;

import dagger.Subcomponent;

@SuppressWarnings("DefaultFileTemplate")
@PerActivity
@Subcomponent(modules = Module.class)
public interface Component {
    void inject(OldProducts fragment);
}