package com.webaspiration.mechanics.Ui.ViewProduct;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.webaspiration.mechanics.Adapter.ImageSliderAdapter;
import com.webaspiration.mechanics.Dagger.Provides.Common;
import com.webaspiration.mechanics.Dagger.Provides.Fonts;
import com.webaspiration.mechanics.Network.Beans.GetProductResponse;
import com.webaspiration.mechanics.Network.Beans.Product;
import com.webaspiration.mechanics.Network.Beans.ProductImage;
import com.webaspiration.mechanics.R;
import com.webaspiration.mechanics.Ui.Base.BaseActivity;
import com.webaspiration.mechanics.Ui.Chat.ChatActivity;

import java.util.List;
import static java.lang.String.format;
import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class ViewProduct extends BaseActivity implements MVP.View{

    Product product;
    List<ProductImage> arrayList;

    @Inject
    MVP.Presentor presentor;

    @Inject
    Fonts fonts;

    @BindView(R.id.pager)
    ViewPager viewpager;

    @BindView(R.id.viewPagerCountDots)
    LinearLayout imageSliderDotsLayout;

    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.price)
    TextView price;
    @BindView(R.id.des)
    TextView des;
    @BindView(R.id.postedBy)
    TextView postedBy;

    @Override
    public void initialize() {
        overridePendingTransition(R.anim.in_from_right,R.anim.out_to_left);
        product= Common.getGson().fromJson(getIntent().getStringExtra("product"),Product.class);
        arrayList=product.getProductImages();
        presentor.setView(this);
        ((TextView)findViewById(R.id.title)).setText(product.getName());
        ((TextView)findViewById(R.id.title)).setTypeface(fonts.small);
        List<ProductImage> imageSliderList=product.getProductImages();
        viewpager.setAdapter(new ImageSliderAdapter(this,product.getProductImages()));
        com.webaspiration.mechanics.Common.Common.drawPageSelectionIndicators(ViewProduct.this, 0, imageSliderList.size(), imageSliderDotsLayout);
        viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                com.webaspiration.mechanics.Common.Common.drawPageSelectionIndicators(ViewProduct.this, position, imageSliderList.size(), imageSliderDotsLayout);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        name.setTypeface(fonts.small);
        price.setTypeface(fonts.small);
        des.setTypeface(fonts.small);
        postedBy.setTypeface(fonts.small);
        name.setText(Html.fromHtml(format("<b><font color='#444444'>Title :</font></b> <font color='#999999'>%s</font>",product.getName())));
        postedBy.setText(Html.fromHtml(format("<b><font color='#444444'>Posted by :</font></b> <font color='#999999'></font>")));
        price.setText(Html.fromHtml(format("<b><font color='#444444'>Price :</font></b> <font color='#999999'>₹ %s</font>",product.getPrice())));
        des.setText(Html.fromHtml(format("<b><font color='#444444'>Description: </font></b> <font color='#999999'>%s</font>",product.getDescription())));
    }

    @Override
    public int layout() {
        return R.layout.activity_view_product;
    }

    @Override
    public void dependency() {
        component().plus(new Module()).inject(this);
    }

    @Override
    public void networkcall() {
        presentor.init();
    }

    @Override
    public String id() {
        return product.getId().toString();
    }

    @BindView(R.id.chat)
    Button chat;

    @Override
    public void load(GetProductResponse response) {
        postedBy.setText(Html.fromHtml(format("<b><font color='#444444'>Posted by :</font></b> <font color='#999999'>%s</font>",response.getProduct().getUser().getName())));
        if(response.getIsuser()){
            chat.setText("Delete Ad");
            chat.setOnClickListener(view -> presentor.delete());
            chat.setVisibility(View.VISIBLE);
        }else{
            chat.setVisibility(View.VISIBLE);
            chat.setOnClickListener(view -> {
                Intent i=new Intent(this, ChatActivity.class);
                i.putExtra("id",response.getProduct().getId().toString());
                i.putExtra("uid",response.getProduct().getUserId().toString());
                startActivity(i);
            });
        }
    }

    @OnClick(R.id.goback)
    public void goback(){
        onBackPressed();
        finish();
    }

    @Override
    public void message(String message) {
        if (toast.snackbar != null && toast.snackbar.isShownOrQueued()) {
            toast.snackbar.dismiss();
        }
        if (coordinatorLayout != null) {
            toast.message(message).on(coordinatorLayout).snackbar().show();
        } else {
            toast.message(message).toast();
        }
    }
}
