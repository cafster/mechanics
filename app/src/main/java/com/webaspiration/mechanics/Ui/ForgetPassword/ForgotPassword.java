package com.webaspiration.mechanics.Ui.ForgetPassword;

import android.app.Activity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.webaspiration.mechanics.Dagger.Provides.Fonts;
import com.webaspiration.mechanics.R;
import com.webaspiration.mechanics.Ui.Base.BaseActivity;

import javax.inject.Inject;

public class ForgotPassword extends BaseActivity implements MVP.View{

    @Inject
    MVP.Presentor presentor;

    @Inject
    Fonts fonts;

    @Override
    public void initialize() {
        presentor.setView(this);
        overridePendingTransition(R.anim.in_from_right,R.anim.out_to_left);
        ((TextView)findViewById(R.id.title)).setTypeface(fonts.bold);
        ((TextView)findViewById(R.id.des)).setTypeface(fonts.small);
        ((TextView)findViewById(R.id.mobileNumber)).setTypeface(fonts.small);
        ((TextView)findViewById(R.id.reset)).setTypeface(fonts.bold);
        ((TextView)findViewById(R.id.reset)).setOnClickListener(view -> {
            presentor.reset();
        });
    }

    @Override
    public int layout() {
        return R.layout.activity_forgot_password;
    }

    @Override
    public void dependency() {
        component().plus(new Module()).inject(this);
    }

    @Override
    public void networkcall() {

    }

    @Override
    public String number() {
        return ((EditText)findViewById(R.id.mobileNumber)).getText().toString();
    }

    @Override
    public void sendBack() {
        toast.message("New Password has been sent to your mobile number!").on(coordinatorLayout).snackbar().show("Ok", () -> onBackPressed());

    }

    @Override
    public void message(String message) {
        if (toast.snackbar != null && toast.snackbar.isShownOrQueued()) {
            toast.snackbar.dismiss();
        }
        if (coordinatorLayout != null) {
            toast.message(message).on(coordinatorLayout).snackbar().show();
        } else {
            toast.message(message).toast();
        }
    }
}
