package com.webaspiration.mechanics.Ui.Home.Profile;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.webaspiration.mechanics.Adapter.NavDrawerItem;
import com.webaspiration.mechanics.Adapter.ProfileItemAdapter;
import com.webaspiration.mechanics.Dagger.Provides.Fonts;
import com.webaspiration.mechanics.Dagger.Provides.SessionManager;
import com.webaspiration.mechanics.Dagger.Retention.ActivityContext;
import com.webaspiration.mechanics.R;
import com.webaspiration.mechanics.Ui.Base.BaseFragment;
import com.webaspiration.mechanics.Ui.Splash.Splash;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class Profile extends BaseFragment implements MVP.View {


    com.webaspiration.mechanics.Ui.Home.MVP.View view;

    @Inject
    MVP.Presentor presentor;

    @Inject
    Fonts fonts;

    @Inject
    @ActivityContext
    Context context;

    @Override
    public void message(String message) {
        view.message(message);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        view=(com.webaspiration.mechanics.Ui.Home.MVP.View)getActivity();
        view.comp().plus(new Module()).inject(this);
        presentor.setView(this);
    }


    @BindView(R.id.profileImage)
    ImageView profileImage;


    @Override
    protected void onconnected() {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presentor.destroy();
    }

    @Inject
    SessionManager sessionManager;

    @BindView(R.id.mobile)
    TextView mobile;
    @BindView(R.id.name)
    TextView name;

    @BindView(R.id.listview)
    ListView listView;

    @Override
    protected void oncreateview(View v) {
        ((TextView)v.findViewById(R.id.title)).setTypeface(fonts.small);
        if(!sessionManager.image().equals("")){
            Picasso.with(context).load(sessionManager.image()).error(R.drawable.placeholder_men).into(profileImage);
        }
        name.setTypeface(fonts.bold);
        mobile.setTypeface(fonts.small);
        name.setText(sessionManager.name());
        mobile.setText(sessionManager.mobile());
        ArrayList<NavDrawerItem> arrayList=new ArrayList<>();
        arrayList.add(new NavDrawerItem("Profile",R.drawable.users));
        arrayList.add(new NavDrawerItem("Refer to friend",R.drawable.share));
        arrayList.add(new NavDrawerItem("Rate Machine Becho",R.drawable.favorite));
        arrayList.add(new NavDrawerItem("Logout",R.drawable.logout));
        listView.setAdapter(new ProfileItemAdapter(arrayList,fonts.small));
        listView.setOnItemClickListener((adapterView, view1, i, l) -> {

            switch (i){
                case 0:
                    startActivity(new Intent(context, com.webaspiration.mechanics.Ui.Settings.Profile.class));
                    break;
                case 1: {
                    String appPackageName = context.getPackageName();
                    String textToSend = "Hey!! Please download this awesome app to find deals near you  .\nClick: " + "https://play.google.com/store/apps/details?id=" + appPackageName;
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, textToSend);
                    sendIntent.setType("text/plain");
                    startActivity(Intent.createChooser(sendIntent, "Please Choose App"));
                }
                break;
                case 2: {
                    final String appPackageName = context.getPackageName();
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                }

                case 3:
                    sessionManager.logout();
                    startActivity(new Intent(context, Splash.class));
                    view.finis();
                    break;

            }

        });
    }

    @Override
    protected int layout() {
        return R.layout.fragment_profile;
    }
}
