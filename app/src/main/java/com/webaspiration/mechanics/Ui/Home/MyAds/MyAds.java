package com.webaspiration.mechanics.Ui.Home.MyAds;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.webaspiration.mechanics.Adapter.ViewPagerAdapter;
import com.webaspiration.mechanics.Dagger.Provides.Fonts;
import com.webaspiration.mechanics.R;
import com.webaspiration.mechanics.Ui.Base.BaseFragment;
import com.webaspiration.mechanics.Ui.Home.MyAds.NewProducts.NewProducts;
import com.webaspiration.mechanics.Ui.Home.MyAds.OldProducts.OldProducts;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyAds extends BaseFragment implements MVP.View {


    com.webaspiration.mechanics.Ui.Home.MVP.View view;

    @Inject
    MVP.Presentor presentor;

    @Override
    public void message(String message) {
        view.message(message);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        view=(com.webaspiration.mechanics.Ui.Home.MVP.View)getActivity();
        view.comp().plus(new Module()).inject(this);
        presentor.setView(this);
    }



    @Override
    protected void onconnected() {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presentor.destroy();
    }

    @BindView(R.id.tabs)
    TabLayout tabs;

    @BindView(R.id.viewpager)
    ViewPager viewpager;


    @Inject
    Fonts fonts;


    @Override
    protected void oncreateview(View v) {
        ((TextView)v.findViewById(R.id.title)).setTypeface(fonts.small);
        ViewPagerAdapter adapter=new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new NewProducts(),"New Products");
        adapter.addFragment(new OldProducts(),"Old Products");
        viewpager.setAdapter(adapter);
        tabs.setupWithViewPager(viewpager);
    }

    @Override
    protected int layout() {
        return R.layout.fragment_my_ads;
    }
}
