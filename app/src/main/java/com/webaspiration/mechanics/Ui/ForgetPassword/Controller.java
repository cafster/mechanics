package com.webaspiration.mechanics.Ui.ForgetPassword;

import com.webaspiration.mechanics.Common.Validates;
import com.webaspiration.mechanics.Dagger.Provides.Client;
import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Model.Error;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class Controller implements MVP.Model {

    CompositeDisposable disposable;

    public Controller() {
        disposable = new CompositeDisposable();
    }

    @Override
    public void resetCode(String number, NetworkInteractor<Error> handler) {
        disposable.add(Client.mClient.reset(number).flatMap(new Validates())
                .observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
                .subscribe(handler::handle,handler::error));
    }

    @Override
    public void destroy() {
        if (!disposable.isDisposed())
            disposable.dispose();
    }

}