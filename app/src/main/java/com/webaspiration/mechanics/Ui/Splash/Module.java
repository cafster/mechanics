package com.webaspiration.mechanics.Ui.Splash;


import com.webaspiration.mechanics.Dagger.Provides.SessionManager;
import com.webaspiration.mechanics.Dagger.Retention.PerActivity;

import dagger.Provides;

/**
 * Created by shaan on 10/3/2017.
 */

@dagger.Module
public class Module {

    @Provides
    @PerActivity
    MVP.Presentor presentor(SessionManager sessionManager){
        return new Presentor(sessionManager);
    }

}
