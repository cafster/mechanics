package com.webaspiration.mechanics.Ui.LocationSearch;


import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Place.Beans.PlacePrediction;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;


public class LocationPresentor implements MVP.Presentor {

    MVP.Model model;
    MVP.View view;

    public LocationPresentor(MVP.Model model) {
        this.model = model;
    }

    @Override
    public void destroy() {
        model.destroy();
    }

    @Override
    public void search() {
        if(view.filter()==null || view.filter().equals(""))
        model.search(view.search(), new NetworkInteractor<PlacePrediction>() {
            @Override
            public void handle(Response<PlacePrediction> o) {
                if(o.isSuccessful()){
                    Observable.fromIterable(o.body().getPredictions()).map(prediction -> prediction.getDescription()).subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread()).toList().subscribe(view::showList, Throwable::printStackTrace);
                }else{
                    view.message("Error Occured");
                }
            }

            @Override
            public void error(Throwable e) {
                view.message(e.getMessage());
            }
        });
        else{
            model.search(view.search(),view.filter(), new NetworkInteractor<PlacePrediction>() {
                @Override
                public void handle(Response<PlacePrediction> o) {
                    if(o.isSuccessful()){
                        Observable.fromIterable(o.body().getPredictions()).map(prediction -> prediction.getDescription()).subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread()).toList().subscribe(view::showList, Throwable::printStackTrace);
                    }else{
                        view.message("Error Occured");
                    }
                }

                @Override
                public void error(Throwable e) {
                    view.message(e.getMessage());
                }
            });
        }
    }

    @Override
    public void setView(MVP.View c) {
        this.view=c;
    }
}
