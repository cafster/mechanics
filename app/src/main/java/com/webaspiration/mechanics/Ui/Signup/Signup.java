package com.webaspiration.mechanics.Ui.Signup;

import android.Manifest;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.webaspiration.mechanics.Common.SendPlivoMessage;
import com.webaspiration.mechanics.Dagger.Provides.Fonts;
import com.webaspiration.mechanics.Events.SmsRecievedEvent;
import com.webaspiration.mechanics.R;
import com.webaspiration.mechanics.Service.SmsReciever;
import com.webaspiration.mechanics.Ui.Base.BaseActivity;
import com.webaspiration.mechanics.Ui.Login.LoginActivity;
import com.webaspiration.mechanics.View.PinView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import butterknife.BindView;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

import static android.provider.Telephony.Sms.Intents.SMS_RECEIVED_ACTION;

@RuntimePermissions
public class Signup extends BaseActivity implements MVP.View,View.OnClickListener{

    @BindView(R.id.signup_name)
    EditText signup_name;

    @BindView(R.id.signup_number)
    EditText signup_number;

    @BindView(R.id.signup_email)
    EditText signup_email;

    @BindView(R.id.login_password)
    EditText signup_password;

    @BindView(R.id.signup_attempt)
    Button signup_attempt;

    @BindView(R.id.signup_login)
    TextView signup_login;

    @Inject
    Fonts fonts;

    @Inject
    MVP.Presentor presentor;


    @BindView(R.id.login_bottom_image)
    ImageView login_bottom_image;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presentor.destroy();
        removeListener();
    }

    void removeListener(){
        if (isreciverregistered) {
            isreciverregistered = false;
            this.unregisterReceiver(reciever);
        }
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }

    @BindView(R.id.signUpArea)
    LinearLayout signUpArea;

    @BindView(R.id.otpArea)
    RelativeLayout otpArea;


    @Override
    public void initialize() {
        overridePendingTransition(R.anim.in_from_right,R.anim.out_to_left);
        presentor.setView(this);
        signup_name.setTypeface(fonts.small);
        signup_password.setTypeface(fonts.small);
        signup_email.setTypeface(fonts.small);
        signup_number.setTypeface(fonts.small);
        signup_attempt.setTypeface(fonts.small);
        signup_login.setTypeface(fonts.bold);
        change.setOnClickListener(this);
        ((FloatingActionButton)findViewById(R.id.myFAB)).setOnClickListener(this);
        Glide.with(this).load(R.drawable.login_bottom_background).into(login_bottom_image);
        ((TextView)findViewById(R.id.signup_login_des)).setTypeface(fonts.small);
        ((TextView)findViewById(R.id.signup_title)).setTypeface(fonts.bold);
        signup_login.setOnClickListener(view -> {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        });
        signup_attempt.setOnClickListener(view -> presentor.sendOtp());
    }

    @Override
    public String name() {
        return signup_name.getText().toString();
    }

    @Override
    public String email() {
        return signup_email.getText().toString();
    }

    @Override
    public String mobile() {
        return signup_number.getText().toString();
    }

    @Override
    public String password() {
        return signup_password.getText().toString();
    }

    @Override
    public void sendtologin() {
        toast.message("Signup sucessful! Please login to continue").toast();
        startActivity(new Intent(this,LoginActivity.class));
        finish();
    }

    @Override
    public int layout() {
        return R.layout.activity_signup;
    }

    @Override
    public void dependency() {
        component().plus(new Module()).inject(this);
    }

    @Override
    public void networkcall() {

    }

    @Override
    public void message(String message) {
        if (toast.snackbar != null && toast.snackbar.isShownOrQueued()) {
            toast.snackbar.dismiss();
        }
        if (coordinatorLayout != null) {
            toast.message(message).on(coordinatorLayout).snackbar().show();
        } else {
            toast.message(message).toast();
        }
        try {
            otpView.clear();
        }catch (ArrayIndexOutOfBoundsException ignored){

        }
    }

    @Override
    public void hideOtp() {
        otpArea.setVisibility(View.GONE);
        signUpArea.setVisibility(View.VISIBLE);
    }

    @Override
    public void showOtp() {
        resend.setOnClickListener(this);
        otpArea.setVisibility(View.VISIBLE);
        signUpArea.setVisibility(View.GONE);
    }

    int _a = 30;
    int _b = 30;


    @BindView(R.id.pinview)
    PinView otpView;
    @BindView(R.id.otp_resend)
    Button resend;
    @BindView(R.id.otp_change)
    Button change;

    protected boolean isreciverregistered = false;
    protected boolean ispermission = false;
    SmsReciever reciever;

    CountDownTimer countDownTimer;
    void disableresend() {
        resend.setOnClickListener(null);
        resend.setText(String.format("Resend in %s",_a));
        resend.setBackgroundColor(Color.parseColor("#bbbbbb"));
        countDownTimer = new CountDownTimer(_a * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                resend.setText(String.format("Resend in %s",String.valueOf(millisUntilFinished / 1000)));
            }

            @Override
            public void onFinish() {
                resend.setOnClickListener(Signup.this);
                resend.setText("Resend Otp");
                resend.setBackgroundColor(getResources().getColor(R.color.secondary));
                _a = _a + _b;
            }
        };
        countDownTimer.start();
    }

    void changeNumber(){
        _a=_b;
        resend.setText(String.format("Resend in %s",_a));
        resend.setBackgroundColor(Color.parseColor("#bbbbbb"));
        signup_number.setText("");
        hideOtp();
    }

    String otp="";

    @Override
    public void sendOtp() {
        otp= SendPlivoMessage.generateAndSend(mobile());
        showOtp();
        SignupPermissionsDispatcher.installrecieverWithCheck(this);
        disableresend();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        SignupPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.otp_resend:
                otp= SendPlivoMessage.generateAndSend(mobile());
                disableresend();
                break;
            case R.id.otp_change:
                changeNumber();
                removeListener();
                break;
            case R.id.myFAB:
                if(otp.equals(otpView.getValue()))
                    presentor.signUp();
                else
                    message("Please enter a valid otp");
                break;
        }
    }


    @NeedsPermission({Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS})
    public void installreciever() {
        reciever = new SmsReciever();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SMS_RECEIVED_ACTION);
        registerReceiver(reciever, intentFilter);
        isreciverregistered = true;
        ispermission = true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSmsRecievedEvent(SmsRecievedEvent event) {
        otpView.setValue(event.getOtp());
        if(otp.equals(event.getOtp()))
            presentor.signUp();
    }

}
