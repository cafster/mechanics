package com.webaspiration.mechanics.Ui.PostProduct;

import com.nguyenhoanglam.imagepicker.model.Image;
import com.webaspiration.mechanics.Dagger.Provides.SessionManager;
import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Beans.CategoryResponse;
import com.webaspiration.mechanics.Network.Beans.Product;
import com.webaspiration.mechanics.Network.Beans.ProductPostResponse;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import id.zelory.compressor.Compressor;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

/**
 * Created by darkapple on 05/11/17.
 */

public class Presentor implements MVP.Presentor{

    MVP.View view;
    MVP.Model model;


    public Presentor(MVP.Model model) {
        this.model = model;
    }

    @Override
    public void setView(MVP.View c) {
        this.view=c;
    }

    @Override
    public void destroy() {
        model.destroy();
    }

    @Override
    public void init() {
        model.load(new NetworkInteractor<CategoryResponse>() {
            @Override
            public void handle(Response<CategoryResponse> o) {
                if(o.isSuccessful())
                    view.load(o.body());
            }

            @Override
            public void error(Throwable e) {

            }
        });
    }

    @Override
    public void post() {
        if(view.category()==0){
            view.message("Please select a valid category");
            return;
        }
        if(view.name().length()<4){
            view.message("Please enter a valid name");
            return;
        }
        if(view.descriptions().length()<4)
        {
            view.message("Please enter a valid description");
            return;
        }
        if(view.price().length()<2){
            view.message("Please enter a valid price");
            return;
        }
        if(view.images().size()==0)
        {
            view.message("Please select one image atleast");
            return;
        }
        HashMap<String,String>map=new HashMap<>();
        map.put("name",view.name());
        map.put("category",view.category().toString());
        map.put("description",view.descriptions());
        map.put("price",view.price());
        map.put("lat",model.lat());
        map.put("lng",model.lng());
        model.postProduct(map, new NetworkInteractor<ProductPostResponse>() {
            @Override
            public void handle(Response<ProductPostResponse> o) {
                if(o.isSuccessful()){
                    Product product=o.body().getProduct();
                    Compressor compressor = new Compressor(view.c());
                    for(Image image:view.images())
                    {
                        String imagepath=image.getPath();
                        compressor.setMaxWidth(640)
                                .setMaxHeight(480)
                                .setQuality(75).compressToFileAsFlowable(new File(imagepath))
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(file1 -> {
                                    RequestBody requestFile =
                                            RequestBody.create(MediaType.parse("multipart/form-data"), file1);
                                    MultipartBody.Part _body =
                                            MultipartBody.Part.createFormData("image", file1.getName(), requestFile);
                                    model.postImage(product.getId().toString(),_body);
                                },throwable -> {
                                    File file1=new File(imagepath);
                                    RequestBody requestFile =
                                            RequestBody.create(MediaType.parse("multipart/form-data"), file1);
                                    MultipartBody.Part _body =
                                            MultipartBody.Part.createFormData("image", file1.getName(), requestFile);
                                    model.postImage(product.getId().toString(),_body);
                                });
                    }
                    view.showMessage();
                }
            }

            @Override
            public void error(Throwable e) {
                view.message(e.getMessage());
            }
        });
    }
}
