package com.webaspiration.mechanics.Ui.Home.Chat;

import com.webaspiration.mechanics.Common.Validates;
import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Beans.ChatResponse.AllChatResponse;
import com.webaspiration.mechanics.Network.Network;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by darkapple on 09/11/17.
 */

public class Controller implements MVP.Model{

    Network mNetwork;
    CompositeDisposable disposable;

    public Controller(Network mNetwork) {
        this.mNetwork = mNetwork;
        disposable=new CompositeDisposable();
    }

    @Override
    public void load(NetworkInteractor<AllChatResponse> handler) {
        disposable.add(mNetwork.getChats().flatMap(new Validates()).subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread()).subscribe(handler::handle,handler::error));
    }

    @Override
    public void destroy() {
        if(!disposable.isDisposed())
            disposable.dispose();
    }
}
