package com.webaspiration.mechanics.Ui.Search;

import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Beans.ProductResponse;

public interface MVP {


    interface View {
        void message(String message);
        void load(ProductResponse response);
        String search();
    }

    interface Model {
        void destroy();
        void search(String search, NetworkInteractor<ProductResponse>handler);
    }

    interface Presentor {
        void destroy();
        void init();
        void search();
        void setView(View c);
    }


}