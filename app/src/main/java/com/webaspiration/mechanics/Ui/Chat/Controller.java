package com.webaspiration.mechanics.Ui.Chat;

import com.webaspiration.mechanics.Common.Validates;
import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Beans.LoadChatResponse.LoadChatResponse;
import com.webaspiration.mechanics.Network.Beans.UnseenResponse.UnseenResponse;
import com.webaspiration.mechanics.Network.Model.Success;
import com.webaspiration.mechanics.Network.Network;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class Controller implements MVP.Model {

    Network mClient;
    CompositeDisposable disposable;

    public Controller(Network mClient) {
        this.mClient = mClient;
        disposable = new CompositeDisposable();
    }

    @Override
    public void unseen(String a, String b, NetworkInteractor<UnseenResponse> handler) {
        disposable.add(mClient.unseen(a,b).flatMap(new Validates()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(handler::handle,handler::error));
    }

    @Override
    public void load(String a, String b, NetworkInteractor<LoadChatResponse> handler) {
        disposable.add(mClient.loadChat(a,b).flatMap(new Validates()).subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread()).subscribe(handler::handle,handler::error));
    }

    @Override
    public void post(String a, String b, String body, NetworkInteractor<Success> handler) {
        disposable.add(mClient.postMessage(a,b,body).flatMap(new Validates()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(handler::handle,handler::error));
    }

    @Override
    public void destroy() {
        if (!disposable.isDisposed())
            disposable.dispose();
    }

}