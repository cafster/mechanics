package com.webaspiration.mechanics.Ui.Home.MyAds.OldProducts;

import com.webaspiration.mechanics.Common.Validates;
import com.webaspiration.mechanics.Dagger.Provides.Client;
import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Beans.ProductResponse;
import com.webaspiration.mechanics.Network.Network;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by darkapple on 11/11/17.
 */

public class Controller implements MVP.Model {

    Network mNetwork;
    CompositeDisposable disposable;

    public Controller(Client client) {
        this.mNetwork = client.mClient;
        disposable=new CompositeDisposable();
    }

    @Override
    public void load(NetworkInteractor<ProductResponse> handler) {
        disposable.add(mNetwork.oldProducts().flatMap(new Validates()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(handler::handle,handler::error));
    }
    @Override
    public void destroy() {
        if(!disposable.isDisposed())
            disposable.dispose();
    }
}
