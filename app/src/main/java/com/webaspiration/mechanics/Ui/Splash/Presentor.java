package com.webaspiration.mechanics.Ui.Splash;

import com.webaspiration.mechanics.Dagger.Provides.SessionManager;

/**
 * Created by darkapple on 05/11/17.
 */

public class Presentor implements MVP.Presentor {
    MVP.View view;
    SessionManager sessionManager;

    public Presentor(SessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }

    @Override
    public void setView(MVP.View c) {
        this.view = c;
    }

    @Override
    public void init() {
        if (sessionManager.loggedin()) {
            if (sessionManager.getLocation() == null)
                view.sendToLocation();
            else
                view.sendToHome();
        } else {
            view.sendToLogin();
        }
    }
}
