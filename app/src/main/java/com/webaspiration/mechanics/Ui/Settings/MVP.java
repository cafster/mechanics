package com.webaspiration.mechanics.Ui.Settings;

import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Beans.MeResponse.MeResponse;

public interface MVP {


    interface View {
        void message(String message);
        void load(MeResponse response);
    }

    interface Model {
        void destroy();
        void me(NetworkInteractor<MeResponse>responseNetworkInteractor);
    }

    interface Presentor {
        void destroy();
        void init();
        void setView(View c);
    }


}