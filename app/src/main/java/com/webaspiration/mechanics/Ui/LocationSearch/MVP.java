package com.webaspiration.mechanics.Ui.LocationSearch;


import com.webaspiration.mechanics.Interface.NetworkInteractor;

import java.util.List;

public interface MVP {

    interface View {
        void message(String message);
        String search();
        void showList(List<String> list);
        String filter();
    }

    interface Model {
        void destroy();
        void search(String input, String filter, NetworkInteractor a);
        void search(String input, NetworkInteractor a);
    }

    interface Presentor {
        void destroy();
        void setView(View c);
        void search();
    }

}
