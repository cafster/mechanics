package com.webaspiration.mechanics.Ui.Signup;

import com.webaspiration.mechanics.Interface.NetworkInteractor;
import com.webaspiration.mechanics.Network.Model.Success;

import java.util.HashMap;

import retrofit2.Response;

/**
 * Created by darkapple on 05/11/17.
 */

public class Presentor implements MVP.Presentor{

    MVP.View view;
    MVP.Model model;


    public Presentor(MVP.Model model) {
        this.model = model;
    }

    @Override
    public void setView(MVP.View c) {
        this.view=c;
    }

    @Override
    public void destroy() {
        model.destroy();
    }

    @Override
    public void sendOtp() {
        if(view.name().length()<4){
            view.message("Please enter a valid name");
            return;
        }
        if(view.mobile().length()<9){
            view.message("Please enter a valid mobile");
            return;
        }
        if(view.password().length()<4){
            view.message("Please enter a valid password");
            return;
        }
        view.sendOtp();
    }

    @Override
    public void signUp() {
        if(view.name().length()<4){
            view.message("Please enter a valid name");
            return;
        }
        if(view.mobile().length()<9){
            view.message("Please enter a valid mobile");
            return;
        }
        if(view.password().length()<4){
            view.message("Please enter a valid password");
            return;
        }
        HashMap<String,String>map=new HashMap<>();
        map.put("name",view.name());
        map.put("email",view.email());
        map.put("password",view.password());
        map.put("mobile",view.mobile());
        model.signUp(map, new NetworkInteractor<Success>() {
            @Override
            public void handle(Response<Success> o) {
                if(o.isSuccessful()){
                    view.sendtologin();
                }
            }

            @Override
            public void error(Throwable e) {
                view.message(e.getMessage());
            }
        });
    }
}
