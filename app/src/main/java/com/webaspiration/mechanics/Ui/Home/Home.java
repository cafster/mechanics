package com.webaspiration.mechanics.Ui.Home;


import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.webaspiration.mechanics.Common.BottomNavigationViewHelper;
import com.webaspiration.mechanics.Common.Common;
import com.webaspiration.mechanics.Dagger.Component.ActivityComponent;
import com.webaspiration.mechanics.R;
import com.webaspiration.mechanics.Service.Location;
import com.webaspiration.mechanics.Ui.Base.BaseActivity;
import com.webaspiration.mechanics.Ui.Home.Chat.Chat;
import com.webaspiration.mechanics.Ui.Home.Main.Main;
import com.webaspiration.mechanics.Ui.Home.MyAds.MyAds;
import com.webaspiration.mechanics.Ui.Home.Profile.Profile;
import com.webaspiration.mechanics.Ui.PostProduct.PostProduct;

import butterknife.BindView;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class Home extends BaseActivity implements MVP.View {

    @BindView(R.id.navigation)
    BottomNavigationView navigation;

    @BindView(R.id.post_button)
    FloatingActionButton post_button;

    boolean isLocation = false;

    @Override
    public void startLocation() {
        Common.gpsOn(this);
        HomePermissionsDispatcher.requestlocationWithCheck(this);
    }

    @Override
    public void endLocation() {
        stopLocation();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopLocation();
    }

    @Override
    public void finis() {
        finish();
    }

    @Override
    public void hideFabButton() {
        post_button.hide();
    }

    @Override
    public void showFabButton() {
        post_button.show();
    }

    @Override
    public boolean shown() {
        return post_button.isShown();
    }

    void stopLocation() {
        if (isLocation)
            stopService(new Intent(this, Location.class));
        isLocation = false;
    }

    @NeedsPermission({Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
    void requestlocation() {
        startService(new Intent(this, Location.class));
        isLocation = true;
    }

    @OnShowRationale({Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
    void showRationaleForLocation(final PermissionRequest request) {
        new AlertDialog.Builder(this)
                .setMessage(R.string.permission_camera_rationale)
                .setPositiveButton(R.string.button_allow, (dialog, button) -> request.proceed())
                .setNegativeButton(R.string.button_deny, (dialog, button) -> request.cancel())
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        HomePermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }


    @Override
    public void initialize() {
        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
        BottomNavigationViewHelper.disableShiftMode(navigation);
        navigation.setOnNavigationItemSelectedListener(item -> {
            Fragment selectedFragment = null;
            switch (item.getItemId()) {
                case R.id.home:
                    selectedFragment = new Main();
                    break;
                case R.id.chat:
                    selectedFragment = new Chat();
                    break;
                case R.id.ads:
                    selectedFragment = new MyAds();
                    break;
                case R.id.profile:
                    selectedFragment = new Profile();
                    break;
            }
            if (selectedFragment == null)
                return true;
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_container, selectedFragment);
            transaction.setCustomAnimations(R.anim.in_from_left, R.anim.out_to_right);
            transaction.commit();
            return true;
        });
        post_button.setOnClickListener(view -> startActivity(new Intent(this, PostProduct.class)));
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, new Main());
        transaction.commit();
        startLocation();
    }

    @Override
    public int layout() {
        return R.layout.activity_home;
    }

    @Override
    public void dependency() {
        component().inject(this);
    }

    @Override
    public void networkcall() {

    }

    @Override
    public void message(String message) {
        if (toast.snackbar != null && toast.snackbar.isShownOrQueued()) {
            toast.snackbar.dismiss();
        }
        if (coordinatorLayout != null) {
            toast.message(message).on(coordinatorLayout).snackbar().show();
        } else {
            toast.message(message).toast();
        }
    }

    @Override
    public ActivityComponent comp() {
        return component();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
